#!/bin/bash
function pause(){
   read -p "$*"
}
 

now=`date`
site_id='blossom2'
#branch=`git symbolic-ref --short -q HEAD`


branch=$1
if [ -z "$2" ]
	then
		branch='master'
	else
		branch=$1
fi
echo "pushing $branch to production:"
echo "deleting & creating temp branch $site_id-prod from $branch"
git branch -D $site_id-prod
#git checkout $branch
git checkout -b $site_id-prod
#echo "copying some $site_id specific files..."
#cp ads.txt.$site_id ads.txt
#echo "done"
#echo "canceling some .gitignores entries..."
#cp -f wp-content/themes/everandivy/.gitignore.prod wp-content/themes/soyummy/.gitignore
cd wp-content/themes/blossom
npm install
npm run build
#composer clearcache
#composer install
#echo "deleting inner git directory if exsists "
#rm -rf wp-content/themes/soyummy/vendor/psr/http-message/.git
cd ../../../
git add .
git commit -m "auto-build - $now"
git tag $1
echo "pushing to production..."
git push -o nolint $site_id-production $site_id-prod:master --force
git checkout $branch
#git branch -D $site_id-prod
echo "done"