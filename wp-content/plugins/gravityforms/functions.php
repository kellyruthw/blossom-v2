<?php
	add_filter( 'manage_edit-invoices_columns', 'my_edit_invoices_columns' ) ;
	
	function my_edit_invoices_columns( $columns ) {
	
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'title' => __( 'Transaction ID' ),
			'status' => __( 'Status' ),
			'date' => __( 'Date' )
		);
	
		return $columns;
	}

	add_action( 'manage_invoices_posts_custom_column', 'my_manage_invoices_columns', 10, 2 );
	
	function my_manage_invoices_columns( $column, $post_id ) {
		global $post;
	
		switch( $column ) {
	
			case 'status' :
				$paid = get_post_meta($post_id, 'paid', true);
				$date = get_post_meta($post_id, 'paid_date', true);
				

				if ( empty( $paid ) )
					echo 'Pending Transaction';
				else
					echo 'Completed Transaction (' . $date . ')';
	
				break;
			default :
				break;
		}
	}

	add_filter('show_admin_bar', '__return_false');
	add_action( 'init', 'create_posttype' );
	
	include('framework/php/scripts.php');
	include('framework/php/invoice.php');
	
	function create_posttype() {
		register_post_type('invoices',
			array(
		  		'labels' => array(
				'name' => __( 'Invoices' ),
				'singular_name' => __('Invoice')
		  	),
		  	'public' => true,
		  	'has_archive' => true,
		  	'rewrite' => array('slug' => 'invoices'),
			'supports' => array( 'title')
			)
		);
	}
	
	add_action('generate_rewrite_rules', 'work_list');
	
	function work_list($wp_rewrite) {
		$newrules = array();
		$newrules['payment/?([0-9]{1,})/?$'] = 'index.php?page_name=invoice&trans_id=$matches[1]';
		$wp_rewrite->rules = $newrules + $wp_rewrite->rules;
	}
	
	add_filter( 'wp_insert_post_data' , 'modify_post_title' , '99', 2 ); // Grabs the inserted post data so you can modify it.
	
	function modify_post_title( $data ){
	  if (($data['post_type'] == 'invoices') && ($_POST['publish'] != '')) { // If the actual field name of the rating date is different, you'll have to update this.
			$seed = str_split('ABCDEFGHIJKLMNOPQRSTUVWXYZ'); // and any other characters
			shuffle($seed); // probably optional since array_is randomized; this may be redundant
			$rand = '';
			foreach (array_rand($seed, 3) as $k) $rand .= $seed[$k];

			$date = $rand . '-' . date('mdYHis') . '-' . $_POST['post_ID'];
			$title = $date;
			$data['post_title'] =  $title ; //Updates the post title to your new title.
			
			update_post_meta($_POST['post_ID'], 'trans_id', $title);
		
			$products = $_POST['products'];
			
			$prods = '';
			
			for ($i = 0; $i < count($products['product']); $i++) {
				$product = $products['product'][$i];
				$quan 	= $products['quan'][$i];
				$price   = $products['price'][$i];
				
				$prods[] = $products['product'][$i] . ' (x' . $quan . ')'; 				
				$total  += $price;
			}
			
			$to      = $_POST['c_email'];
			$subject = 'Text Mary - Transaction #' . $title;
			$sender  = get_option('name');
					
			$message = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
						<div bgcolor="#9242e3" style="width:100%; margin: 0; padding: 0; background-color: #9242e3">
							<table style="padding: 20px" width="600" align="center" border="0" cellpadding="10" cellspacing="0">
							<tr>
								<td style="color: #fff; font-size: 30px; text-align: center; font-family:Verdana, Geneva, sans-serif"><img src="' . get_bloginfo('template_url') . '/img/logo1.png"></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td style="color: #fff; font-size: 25px; font-family:Verdana, Geneva, sans-serif; font-weight: 600; line-height: 44px;">Hi There!<br />Thank you for your order</td>
							</tr>
							<tr>
								<td>
									<table cellpadding="10" cellspacing="0" border="0" style="border: 2px solid #fff" width="100%">
										<tr>
											<td style="color: #fff; font-size: 18px; font-family:Verdana, Geneva, sans-serif">Item(s) Purchased: ' . implode(', ', $prods) . '</td>
										</tr>
										<tr>
											<td style="color: #fff; font-size: 18px; font-family:Verdana, Geneva, sans-serif">Amount: $' . $total . '</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td style="color: #fff; font-size: 18px; font-family:Verdana, Geneva, sans-serif">
									Click on the link below to make a donation:<br><br>
									<a href="' . get_bloginfo('url') . '/invoices/' . $title . '/" style="color: #fff">
										' . get_bloginfo('url') . '/invoices/' . $title . '/
									</a>
									<br><br>
								</td>
							</tr>
							<tr>
								<td style="color: #fff; font-size: 18px; font-family:Verdana, Geneva, sans-serif">Best,<br />
									The Text Mary Team</td>
							</tr>
							<tr>
								<td>
									<table cellspacing="0" border="0" style="border-top: 1px solid #fff" width="100%">
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td style="color: #fff; font-size: 14px; font-family:Verdana, Geneva, sans-serif" align="center">Our Social Media</td>
										</tr>
										<tr>
											<td style="color: #fff; font-size: 14px; font-family:Verdana, Geneva, sans-serif" align="center">&nbsp;</td>
										</tr>
										<tr>
											<td style="color: #fff; font-size: 14px; font-family:Verdana, Geneva, sans-serif" align="center"><br />
												Text Mary, INC. (C) 2015-2016 All Rights Reserved.</td>
										</tr>
									</table>
								</td>
							</tr>
							</table>
						</div>';
					
			$headers[] = 'MIME-Version: 1.0' . "\r\n";
			$headers[] = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers[] = "X-Mailer: PHP \r\n";
			$headers[] = 'From: Text Mary <info@textmary.com>' . "\r\n";
					
			$mail = wp_mail($to, $subject, $message, $headers);
	  }
	  else {
			$trans_id = get_post_meta($_POST['post_ID'], 'trans_id', true); 
			if ($trans_id != '') {
				$data['post_title'] =  $trans_id;
			}
	  }
	  
	  return $data; // Returns the modified data.
	}
?>