<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package bestsubscriptions
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="content">
				<div class="container">
					<h1>Whoops!</h1>
					<p>This page has exploded. Maybe these will spark your creativity.</p>
					<div class="popular-posts">
					<?php 
						$popularpost = new WP_Query( array( 
						
						'posts_per_page' => 6, 
						'meta_key' => 
						'wpb_post_views_count', 
						'orderby' => 'meta_value_num', 
						'order' => 'DESC'  ) );
						
						while ( $popularpost->have_posts() ) : $popularpost->the_post();
						
						$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );  ?>
				 
							 <article>
							    <a href="<?php echo esc_url( get_permalink() ); ?>" class="featured-image"><?php the_post_thumbnail('feed'); ?></a>
							    <div class="copy">
							        <h3><a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a></h3>
							    </div>
						    </article>
				 
						<?php endwhile; ?>
					</div>

				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
