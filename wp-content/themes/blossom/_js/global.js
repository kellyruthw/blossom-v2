var $hamburger = $(".hamburger");
var $menu = $("header nav");
  $hamburger.on("click", function(e) {
    $hamburger.toggleClass("is-active");
    $menu.show("fast");
  });

$('.articles').infiniteScroll({
  // options
  path: '.next a',
  append: '.articles article',
  prefill: true,
  history: false,
  hideNav: '.navigation',
  button: '.view-more .btn',
// load pages on button click
scrollThreshold: false,
status: '.page-load-status',
// load pages on button click

});


/**
 * main.js
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2016, Codrops
 * http://www.codrops.com
 */
;(function(window) {

	'use strict';

	var mainContainer = document.querySelector('.main-wrap'),
		openCtrl = document.getElementById('btn-search'),
		closeCtrl = document.getElementById('btn-search-close'),
		searchContainer = document.querySelector('.big-search'),
		inputSearch = searchContainer.querySelector('.search-field');

	function init() {
		initEvents();	
	}

	function initEvents() {
		openCtrl.addEventListener('click', openSearch);
		closeCtrl.addEventListener('click', closeSearch);
		document.addEventListener('keyup', function(ev) {
			// escape key.
			if( ev.keyCode == 27 ) {
				closeSearch();
			}
		});
	}

	function openSearch() {
		mainContainer.classList.add('main-wrap--overlay');
		closeCtrl.classList.remove('btn--hidden');
		searchContainer.classList.add('search--open');
		setTimeout(function() {
			inputSearch.focus();
		}, 500);
	}

	function closeSearch() {
		mainContainer.classList.remove('main-wrap--overlay');
		closeCtrl.classList.add('btn--hidden');
		searchContainer.classList.remove('search--open');
		inputSearch.blur();
		inputSearch.value = '';
	}

	init();

})(window);