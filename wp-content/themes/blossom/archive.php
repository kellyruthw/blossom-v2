<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bestsubscriptions
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<h1><?php the_archive_title(); ?></h1>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
			    $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
			    <article>
				    <a href="<?php echo esc_url( get_permalink() ); ?>">
					    <?php the_post_thumbnail('recipe-thumb'); ?>
					</a>
				    <div class="copy">
				        <h2><a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a></h2>
				        <?php the_excerpt(); ?>
				    </div>
			    </article>
			<?php endwhile; else: ?>
			<h2 class="coming-soon">More coming soon!</h2>
			<?php endif; ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
