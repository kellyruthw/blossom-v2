<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bestsubscriptions
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="content">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/icon-blossom-happy.svg" class="happy">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/splatter-orange.png" class="splatter-orange">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/icon-blossom-wink.svg" class="wink">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/splatter-blue.png" class="splatter-blue">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/scissors.png" class="scissors">
				<h1><?php the_archive_title(); ?></h1>
				
				<?php $this_category = get_category($cat); ?>
				<div class="container">
					<div class="articles">
					
							<?php
							// set the "paged" parameter (use 'page' if the query is on a static front page)
							$page = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;
							
							?>
							<?php
							// the loop
							while ( have_posts() ) : the_post(); 
													
							?>
							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							    <a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_post_thumbnail('feed'); ?></a>
							    <div class="copy">
							        <h2><a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a></h2>
							        <?php the_excerpt(); ?>
							    </div>
						    </article>
							<?php endwhile; ?>
							<div class="navigation">						
								<div class="next">
									<?php next_posts_link( 'Older Entries', $the_query2->max_num_pages ); ?>
								</div>
							</div>
							<?php 
							wp_reset_postdata(); 
							?>	
						</div>
						<?php
							if ( wp_is_mobile() ) { ?>
								<div class="view-more mobile">
									<div class="page-load-status">
									  <div class="loader-ellips infinite-scroll-request">
									    <span class="loader-ellips__dot"></span>
									    <span class="loader-ellips__dot"></span>
									    <span class="loader-ellips__dot"></span>
									    <span class="loader-ellips__dot"></span>
									  </div>
									</div>
									<button class="btn">Show me more</button>
								</div>
								
								<?php get_sidebar(); ?>
	
							<?php } else { ?>
								<?php get_sidebar(); ?>
								<div class="view-more">
									<div class="page-load-status">
									  <div class="loader-ellips infinite-scroll-request">
									    <span class="loader-ellips__dot"></span>
									    <span class="loader-ellips__dot"></span>
									    <span class="loader-ellips__dot"></span>
									    <span class="loader-ellips__dot"></span>
									  </div>
									</div>
									<button class="btn">Show me more</button>
								</div>
							<?php }
						?>
						
					</div>
				</div>					
			</div> <!-- content -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>

				