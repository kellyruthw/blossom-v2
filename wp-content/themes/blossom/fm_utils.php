<?php
class Utils {

	private static $instance;

	public static function getInstance()
    {
        if (!isset(self::$instance)) {
			self::$instance = new Utils();
        }
		return self::$instance;
	}

	public function isSponsored() {
		if (!empty($cat)) return false;
		return get_field('is_sponsored') == true;
	}

	public function isHomePage() {
        return is_front_page();
    }

	public function getPageType() {
        if(is_front_page()){
            return 'home';
        }
        
        if (is_category()|| is_tax()) return 'category';
        $res = 'post';
        $post_type = get_post_type();
        if ($post_type != 'post') {
            return $post_type;
        } else {
            $type = get_field('type');
            if ($type == 'simple' || $type == '') {
                $type = 'article';
            }
            return $type;
        }
    }

}
