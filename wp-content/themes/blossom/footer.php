<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bestsubscriptions
 */

?>

	</div><!-- #content -->

	<div class="big-search">
		<?php get_search_form(); ?>
		<div class="instructions">Hit enter to search or ESC to close</div>
		<div class="search__related">
			<div class="search__suggestion">
				<h2>May We Suggest?</h2>
				<?php 
					$popularpost = new WP_Query( array( 
					
					'posts_per_page' => 6, 
					'meta_key' => 
					'wpb_post_views_count', 
					'orderby' => 'meta_value_num', 
					'order' => 'DESC'  ) );
					
					while ( $popularpost->have_posts() ) : $popularpost->the_post(); ?>
								 
						 <article>
						    <a href="<?php echo esc_url( get_permalink() ); ?>" class="featured-image"><?php the_post_thumbnail('sidebar-trending'); ?></a>
						    <div class="copy">
						        <h3><a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a></h3>
						    </div>
					    </article>
			 
				<?php endwhile; ?>

			</div>
		</div>
	</div>
	<button id="btn-search-close" class="btn btn--hidden btn--search-close" aria-label="Close search form"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-close.svg" /></button>


</div><!-- #page -->
<!--<div class="ad"><img src="<?php echo get_template_directory_uri(); ?>/assets/ad.jpg"></div> -->
<footer id="colophon" class="site-footer">
	<div class="container">
	
		<div class="links">
			<div class="socials">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/logo-blossom-white.png" class="logo-footer" />
				<ul>
					<li><a href="https://www.facebook.com/FirstMediaBlossom" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-fb.svg"></li></a>
					<li><a href="https://twitter.com/blossomhacks"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-tw.svg"></li></a>
					<li><a href="https://www.instagram.com/blossom/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-ig.svg"></li></a>
					<li><a href="https://www.pinterest.com/blossomhacks/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-pin.svg"></a></li>
					<li><a href="https://www.youtube.com/channel/UC2WuPTt0k8yDJpfenggOAVQ" target=""><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-yt.svg"></a></li>
				</ul>
			</div>
			<?php wp_nav_menu( array( 'theme_location' => 'menu-2' ) ); ?>
		</div>
		<div class="copyright">
			<?php wp_nav_menu( array( 'theme_location' => 'menu-4' ) ); ?>
			<p>©2019 Blossom All Rights Reserved.</p>
		</div>
	</div>
</footer><!-- #colophon -->


<?php wp_footer(); ?>
<div class="md-overlay"></div>

</body>
</html>
