<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bestsubscriptions
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		<div class="featured-posts triple-hero">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/icon-blossom-happy.svg" class="happy">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/splatter-orange.png" class="splatter-orange">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/icon-blossom-wink.svg" class="wink">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/splatter-blue.png" class="splatter-blue">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/scissors.png" class="scissors">
			<?php 
			$large_hero = get_field('large_hero');
			
			if( $large_hero ): ?>
			    <?php foreach( $large_hero as $post):
				    $do_not_duplicate[] = $post->ID;
				    $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
				     // variable must be called $post (IMPORTANT) ?>
			        <?php setup_postdata($post); ?>
			         <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					    <a href="<?php echo esc_url( get_permalink() ); ?>" class="featured-image"><?php the_post_thumbnail('homepage-large-hero'); ?></a>
					    <div class="copy">
						    <div class="cat">
							   <?php
								   $categories = get_the_category();
								   $term2 = $categories[0]->term_id;
								   $name = $categories[0]->name;
								   $slug = $categories[0]->slug;
								   $category_link2 = get_category_link( $term2 );
								   $img = get_field('cat_image', 'term_'.$term2);
								?>
								<a href="<?php echo esc_url( $category_link2 ); ?>" class="<?php echo $slug; ?>">
									<img src="<?php echo $img; ?>" alt="<?php echo $img; ?>" /><h4><?php echo $name ?></h4>
								</a>
						    </div>
					        <h2><a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a></h2>
					        <?php the_excerpt(); ?>
					    </div>
				    </article>
			    <?php endforeach; ?>
			    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php endif; ?>
			<?php 
			$small_hero = get_field('small_hero');
			
			if( $small_hero ): ?>
			<div class='small-hero'>
			    <?php foreach( $small_hero as $post):
				    $do_not_duplicate[] = $post->ID;
				    $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); // variable must be called $post (IMPORTANT) ?>
			        <?php setup_postdata($post); ?>
			         <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					    <a href="<?php echo esc_url( get_permalink() ); ?>" class="featured-image"><?php the_post_thumbnail('feed'); ?></a>
					    <div class="copy">
						    <div class="cat">
							   <?php
								   $categories = get_the_category();
								   $term2 = $categories[0]->term_id;
								   $name = $categories[0]->name;
								   $slug = $categories[0]->slug;
								   $category_link2 = get_category_link( $term2 );
								   $img = get_field('cat_image', 'term_'.$term2);
								?>
								<a href="<?php echo esc_url( $category_link2 ); ?>" class="<?php echo $slug; ?>">
									<img src="<?php echo $img; ?>" alt="<?php echo $img; ?>" /><h4><?php echo $name ?></h4>
								</a>
						    </div>
					        <h2><a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a></h2>
					    </div>
				    </article>
			    <?php endforeach; ?>
			</div>
			    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php endif; ?>
		</div>	
		<div class="content bg-zigzag">
			<div class="container">
				<div class="cats">
					<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-3',
						'menu_id'        => 'categories',
					) );
					?>
				</div>
				<div class="articles">
			
					<?php
					// set the "paged" parameter (use 'page' if the query is on a static front page)
					$paged = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;
					
					$args2 = array(
					    'posts_per_page' => 10,
					    'post__not_in' => $do_not_duplicate,
					    'paged' => $paged
					);
					
					$the_query2 = new WP_Query($args2); ?>
					
					
					<?php
					// the loop
					while ( $the_query2->have_posts() ) : $the_query2->the_post(); 
					
					$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
					
					?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					    <a href="<?php echo esc_url( get_permalink() ); ?>" class="featured-image">
						    <?php the_post_thumbnail('feed'); ?>
						    <?php if ( in_category('diy') ){ ?>
							   <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-play.svg" class="play">
						    <?php } ?>
					    </a>
					    <div class="copy">
						    <div class="cat">
							   <?php
								   $categories = get_the_category();
								   $term2 = $categories[0]->term_id;
								   $name = $categories[0]->name;
								   $slug = $categories[0]->slug;
								   $category_link2 = get_category_link( $term2 );
								   $img = get_field('cat_image', 'term_'.$term2);
								?>
								<a href="<?php echo esc_url( $category_link2 ); ?>" class="<?php echo $slug; ?>">
									<img src="<?php echo $img; ?>" alt="<?php echo $img; ?>" /><h4><?php echo $name ?></h4>
								</a>
						    </div>
					        <h2><a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a></h2>
					        <?php the_excerpt(); ?>
					    </div>
				    </article>
					<?php endwhile; ?>
					<div class="navigation">						
						<div class="next">
							<?php next_posts_link( 'Older Entries', $the_query2->max_num_pages ); ?>
						</div>
					</div>
					<?php 
					// clean up after the query and pagination
					wp_reset_postdata(); 
					?>
					
				</div>
				<?php
					if ( wp_is_mobile() ) { ?>
						<div class="view-more mobile">
							<div class="page-load-status">
							  <div class="loader-ellips infinite-scroll-request">
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							  </div>
							</div>
							<button class="btn">Show me more</button>
						</div>
						
						<?php get_sidebar(); ?>

					<?php } else { ?>
						<?php get_sidebar(); ?>
						<div class="view-more">
							<div class="page-load-status">
							  <div class="loader-ellips infinite-scroll-request">
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							  </div>
							</div>
							<button class="btn">Show me more</button>
						</div>
					<?php }
				?>
				
			</div>
		</main>
	</div><!-- #primary -->

<?php get_footer(); ?>
