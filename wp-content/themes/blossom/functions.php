<?php
// Let users define their own feed slug.
if ( ! defined( 'CUSTOM_INSTANT_ARTICLES_SLUG' ) ) {
	define( 'CUSTOM_INSTANT_ARTICLES_SLUG', 'fbia' );
}
if ( ! defined( 'CUSTOM_INSTANT_ARTICLES_SLUG_DEV' ) ) {
	define( 'CUSTOM_INSTANT_ARTICLES_SLUG_DEV', 'fbia_dev' );
}
define('__ROOT__', dirname(__FILE__));
include('shortcodes/shortcodes_includes.php');
/**
 * soyummy functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package soyummy
 */

if ( ! function_exists( 'soyummy_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function soyummy_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on soyummy, use a find and replace
		 * to change 'soyummy' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'soyummy', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'homepage-large-hero', 700, 700, true );
		add_image_size( 'single', 700, 410, true );
		add_image_size( 'sidebar-trending', 80, 80, true );
		add_image_size( 'feed', 360, 360, true );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary'),
			'menu-2' => esc_html__( 'Footer'),
			'menu-3' => esc_html__( 'Categories'),
			'menu-4' => esc_html__( 'Copyright'),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'soyummy_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
	}
endif;
add_action( 'after_setup_theme', 'soyummy_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function soyummy_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'soyummy_content_width', 640 );
}
add_action( 'after_setup_theme', 'soyummy_content_width', 0 );


/**
 * Enqueue scripts and styles.
 */
function soyummy_scripts() {
	
	wp_enqueue_style( 'soyummy-style', get_stylesheet_uri() );	
	// wp_enqueue_script( 'soyummy-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
  // 	wp_enqueue_script( 'soyummy-jquery', 'https://code.jquery.com/jquery-2.2.4.min.js', array(), '20151215', true );
  // 	wp_enqueue_script( 'soyummy-infinite', 'https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js', array(), '20151215', true );
	// wp_enqueue_script( 'soyummy-global', get_template_directory_uri() . '/js/global.js', array(), '20151215', true );
  // wp_enqueue_script( 'soyummy-modal', get_template_directory_uri() . '/js/modal.js', array(), '20151215', true );
  wp_register_script('google_dfp_call','https://c.amazon-adsystem.com/aax2/apstag.js');
  wp_register_script('amazon-apstag-javascript-api','https://securepubads.g.doubleclick.net/tag/js/gpt.js');
	$jtime = filemtime( get_template_directory() . '/assets/js/main.min.js' );
	wp_enqueue_script('main_script',get_template_directory_uri().'/assets/js/main.min.js',[ 'google_dfp_call' , 'amazon-apstag-javascript-api'],$jtime ,true);

  

}
add_action( 'wp_enqueue_scripts', 'soyummy_scripts' );

function custom_load_scripts() { 
   // Load if not mobile 
   if (  wp_is_mobile() ) { 
      // Example script 
      wp_enqueue_style( 'soyummy-style-menu', get_template_directory_uri() . '/assets/css/hamburger.min.css');
      } 
}

add_action( 'wp_enqueue_scripts', 'custom_load_scripts' );


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';


add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

            $title = single_cat_title( '', false );

        } elseif ( is_tag() ) {

            $title = single_tag_title( '', false );

        } elseif ( is_author() ) {

            $title = '<span class="vcard">' . get_the_author() . '</span>' ;

        }

    return $title;

});

// Replaces the excerpt "Read More" text by a link
function new_excerpt_more($more) {
       global $post;
	return ' ...</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');


// Reducdes word count in exceprt
function tn_custom_excerpt_length( $length ) {
return 30;
}
add_filter( 'excerpt_length', 'tn_custom_excerpt_length', 999 );

// Add image to menu items
add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);

function my_wp_nav_menu_objects( $items, $args ) {
	
	// loop
	foreach( $items as &$item ) {
		
		// vars
		$icon = get_field('cat_image', $item); ?>
		
		<?php $item->title .= '<img src="'.$icon.'" />'; ?>

		
	<?php }
	
	
	// return
	return $items;
	
}

// Most popular post
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

// adds post count
function wpb_get_post_views($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}

function centil_infinite_scroll($pid){
  if (is_single()) { ?>
    <script type="text/javascript" >
      jQuery(document).ready(function($) {

        $(window).scroll(function() {
          var footerPos = $('footer').last().position().top;
          var pos = $(window).scrollTop();

          // Load next article
          if (pos+(screen.height*4) > footerPos) {
            if ($(".centil-infinite-scroll").first().hasClass('working')) {
              return false;
            } else {
              $(".centil-infinite-scroll").first().addClass('working');
            }

            var centilNextPostId = $(".centil-infinite-scroll").first().text();
            var data = {
              'action': 'centil_is',
              'centilNextPostId': centilNextPostId
            };

            // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
            jQuery.post('<?php echo admin_url("admin-ajax.php"); ?>', data, function(response) {
              $(".centil-infinite-scroll").first().replaceWith(response);
            }, 'html');
          }

          // Update new URL
          var currUrl = $(".centil-post-header").first().attr("url");
          var currTitle = $(".centil-post-header").first().attr("title");

          if ($(".centil-post-header").length > 1 && history.pushState) {
            for (var i=0; i<$(".centil-post-header").length; i++) {
              if (pos+(screen.height/2) >= $(".centil-post-header").eq(i).next().position().top) {
                currUrl = $(".centil-post-header").eq(i).attr("url");
                currTitle = $(".centil-post-header").eq(i).attr("title");
              }
            }
          }
          if (location.href != currUrl) {
            history.pushState({}, currTitle, currUrl);
          }
        });
      });
    </script>

  <?php }
}

add_action( 'wp_foot', 'centil_infinite_scroll' );

function centil_infinite_scroll_callback() {

  if (isset($_POST['centilNextPostId']) && $_POST['centilNextPostId']) {
    // The Query
    $the_query = new WP_Query(array('p'=>$_POST['centilNextPostId']));

    // The Loop
    if ( $the_query->have_posts() ) {
      while ( $the_query->have_posts() ) {
        $the_query->the_post();
        get_template_part( 'template-parts/content', 'single' );
      }
    }
    /* Restore original Post Data */
    wp_reset_postdata();
  }

  wp_die();
}
add_action( 'wp_ajax_centil_is', 'centil_infinite_scroll_callback' );
add_action( 'wp_ajax_nopriv_centil_is', 'centil_infinite_scroll_callback' );

function is_post_type($type){
    global $wp_query;
    if($type == get_post_type($wp_query->post->ID)) 
        return true;
    return false;
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');