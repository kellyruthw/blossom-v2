import {
    ObjectObservable
} from "./utils/object-observe";

export const thirdPartyApiNames = {
    facebook: "fbq",
    googleAnalytics: "ga",
    outbrain: "obApi",
    pubplus: "PPTracker"

};
const windowObservable = new ObjectObservable(window, thirdPartyApiNames.facebook,
    thirdPartyApiNames.googleAnalytics,
    thirdPartyApiNames.outbrain ,
    thirdPartyApiNames.pubplus);
class ObjectWatcher {
    constructor(object) {
        this.object = object;

    }

    push(callback) {
        windowObservable.push(this.object, () => {
            callback();
        });
    }
}

export const fbqWatcher = new ObjectWatcher(thirdPartyApiNames.facebook);
export const gaWatcher = new ObjectWatcher(thirdPartyApiNames.googleAnalytics);
export const obApiWatcher = new ObjectWatcher(thirdPartyApiNames.outbrain);
export const ppWatcher = new ObjectWatcher(thirdPartyApiNames.pubplus);