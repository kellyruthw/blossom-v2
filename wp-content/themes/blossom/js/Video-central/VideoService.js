/* eslint-disable camelcase */
/* eslint-disable class-methods-use-this */
//#region video player proto
import {
    getUrlParams,
    urlParamsToStr
} from './../utils';
import {
    pick,
    forEach,
    isEmpty,
    result
} from 'lodash-es';

import {
    isSponsored
} from '../page-types';
import {
    getUserLast2digit
} from '../user-manager';
class VideoPlayer {

    constructor(playerId, options) {
        this.id = playerId;
        this.options = options;
    }

    //fixes custom values for DFP key values on JW player
    setTargetingKeyVals() {
        let urlParams = getUrlParams();
        let utmParams = pick(urlParams, ['utm_source', 'utm_content', 'utm_medium', 'utm_campaign']);

        utmParams.sponsored = isSponsored;
        utmParams.random_number = getUserLast2digit();
        let targetingKeyVals = urlParamsToStr(utmParams);
        targetingKeyVals = encodeURIComponent(targetingKeyVals);


        let adSchedule = result(window.jwplayer, 'defaults.advertising.schedule', []);
        forEach(adSchedule, (item) => {
            if (!isEmpty(item.tag)) {
                //console.log(item.tag);
                item.tag = item.tag.replace('__KEYS__%3D__VALS__', targetingKeyVals);
            }
        });
        forEach(adSchedule, (item) => {
            console.log(item.tag)
        });
    }

    //given a dom selector creates and attachs an instance of a video player
    createInstance(el) {

        let playerInstance = window.jwplayer(el);
        return playerInstance;
    }
    //endregion

    //inits the player's script, should only run once
    __init() {
        //load script
        let self = this;
        return new Promise((resolve, reject) => {
            let scriptEl = document.createElement('script');
            scriptEl.src = `https://content.jwplatform.com/libraries/${this.id}.js`;
            document.body.appendChild(scriptEl);
            scriptEl.onerror = reject;
            scriptEl.onload = () => {
                self.setTargetingKeyVals();
                resolve(self);
            };
        });
    };
};
//endregion

export default class VideoService {

    constructor() {
        this.initPromises = {};
    }

    createPlayer(playerId, options) {
        if (this.initPromises[playerId]) {
            return this.initPromises[playerId];
        }
        let player = new VideoPlayer(playerId, options);
        // eslint-disable-next-line no-underscore-dangle
        this.initPromises[playerId] = player.__init();
        return this.initPromises[playerId];
    }






    // registerParselyTracker(playerInstance) {
    // 	playerInstance.on('ready', function() {
    // 		console.log('ready');
    // 		playerInstance.on("play", function() {
    // 			console.log('play');
    // 			var playlistItem = playerInstance.getPlaylistItem(playerInstance.getPlaylistIndex());
    // 			PARSELY.video.trackPlay(
    // 				playlistItem.mediaid, {
    // 					title: playlistItem.title,
    // 					image_url: playlistItem.image,
    // 					section: $('.article_title').html(),
    // 					pub_date_tmsp: $('#pubdate').attr('data-value'),
    // 					tags: arr_tags,
    // 					authors: [$('.article_meta .name a').html()]
    // 				});
    // 		});
    // 		playerInstance.on("pause", function() {
    // 			console.log('pause');
    // 			var playlistItem = playerInstance.getPlaylistItem(playerInstance.getPlaylistIndex());
    // 			PARSELY.video.trackPause(playlistItem.mediaid);
    // 		});
    // 		playerInstance.on("stop", function() {
    // 			console.log('stop');
    // 			var playlistItem = playerInstance.getPlaylistItem(playerInstance.getPlaylistIndex());
    // 			PARSELY.video.trackPause(playlistItem.mediaid);
    // 		});
    // 	});
    // }




};