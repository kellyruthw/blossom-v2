import VideoService from "./VideoService";

import trackingBuilder from "../tracking-central";
import {
    pageTypesConsts,
    isRecipePage
} from "../page-types";
import {
    additionalGaCostumEvent
} from "../tracking-central/google-analytics-reporter";
import {
    trackingVersion
} from "../tracking-central/version";
export let videoPlayrs = {};

let videpPageConfig = {};
videpPageConfig[pageTypesConsts.RECIPE] = {
    autoplay: true,
    muteWhenAutoplay: false
};

videpPageConfig[pageTypesConsts.ARTICLE] = {
    autoplay: true,
    muteWhenAutoplay: true
};

async function setupVideo(el, index, autorun = false) {
    let {
        playerId,
        mediaId
    } = el.dataset;
    if (!mediaId) {
        console.log('skipping player since mediaId is undefined');
        return;
    }
    if (!playerId) {
        console.log('skipping player since playerId is undefined');
        return;
    }

    let videoService = new VideoService();
    let player = await videoService.createPlayer(playerId);
    let playerInstance = player.createInstance(el);
    playerInstance.setup({
        playlist: `//cdn.jwplayer.com/v2/media/${mediaId}`,
        autostart: false,
        mute: true
    });

    let wrapperElement = el.parentElement;
    wrapperElement.setAttribute("video-loaded", true);
    wrapperElement.id = playerInstance.id;
    videoPlayrs[playerInstance.id] = playerInstance;
    playerInstance.on('pause', function (event) {
        playerInstance.pauseReason = event.pauseReason;
        if (event.pauseReason === "interaction") {
            trackingBuilder("video_user_stop").send();
        } else {
            //trackingBuilder("video_autostop").send();
        }


        
    });

    playerInstance.on('playAttemptFailed', function (event) {
        let {
            code,
            message,
            name
        } = event.error;
        trackingBuilder("video_err").extras(message, name, code).send();
        
    });

    playerInstance.on('adPause', function () {
        //playerInstance.pauseReason = 'adPause';
        //trackingBuilder("ad_video_paused").extras(event.playReason , playerInstance.id).send();
        trackingBuilder("video_ad_stop").send();
        
    });

    playerInstance.on('adPlay', function () {
        trackingBuilder("video_ad_play").send();
        
    });

    playerInstance.on('adSkipped', function () {
        trackingBuilder("video_ad_skipped").send();
        
    });

    playerInstance.on('adError', function () {
        //trackingBuilder("video_ad_err").extras(event.message).send();
        
    });

    playerInstance.on('adComplete', function (event) {
        trackingBuilder("video_ad_complete").extras(event.message).send();
        
    });


    playerInstance.on('adBlock', function (event) {
        trackingBuilder("video_ad_block").extras(event.message).send();
        
    });

    playerInstance.on('adClick', function (event) {
        trackingBuilder("video_ad_clk").extras(event.message).send();
        
    });

    playerInstance.on('play', function (event) {
        playerInstance.playReason = event.playReason;
        playerInstance.pauseReason = null;
        if (event.playReason === "interaction") {
            //playerInstance.setMute(false);
            trackingBuilder("video_user_play").send();
        } else {
            //playerInstance.setMute(true);
            trackingBuilder("video_autoplay").send();

        }
        additionalGaCostumEvent('send', 'event', "video_watched_" + trackingVersion, document.URL, playerInstance.id);

        
    });

    playerInstance.on('error', (event) => {
        let {
            sourceError,
            message,
            code
        } = event;
        trackingBuilder("video_err").extras(sourceError, message, code).send();

    });

    playerInstance.on('warning', (event) => {
        let {
            sourceError,
            message,
            code
        } = event;
        trackingBuilder("video_warning").extras(sourceError, message, code).send();

    });

    if (isRecipePage && autorun && index == 0) {
        playerInstance.play();
    }
    // playerInstance.setConfig([{
    //     //repeat: true,
    //     autostart: true,
    //     //mute: false,
    //     volume: false
    //   }]);



}

//region helper functions
export function loadVideoIfNeeded(autorun = false, videoSelector = '.multi-recipe-video, .single-recipe-video, .single-post-video ,.single-video ') {

    let els = document.querySelectorAll(videoSelector);
    [...els].forEach((el, index) => {
        let videoLoaded = el.getAttribute("video-loaded");
        if (videoLoaded) {
            return;
        }

        console.log('loadVideoIfNeeded -> started');
        setupVideo(el, index, autorun);

    });

}