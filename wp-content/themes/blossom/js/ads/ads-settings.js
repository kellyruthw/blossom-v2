/* eslint-disable max-lines */
export const dfpSettings = {
    "ei_recipe_d970x250_1": {
        "size": [
            [
                1,
                1
            ],
            [
                970,
                250
            ],
            [
                728,
                90
            ],
            [
                970,
                90
            ]
        ]
    },
    "ei_recipe_d728x90_3": {
        "size": [
            [
                1,
                1
            ],
            [
                728,
                90
            ]
        ]
    },
    "ei_recipe_d300x600_3": {
        "size": [
            [
                1,
                1
            ],
            [
                300,
                250
            ],
            [
                300,
                600
            ]
        ]
    },
    "ei_recipe_m300x250_1": {
        "size": [
            [
                1,
                1
            ],
            [
                320,
                50
            ],
            [
                320,
                100
            ]
        ]
    },
    "ei_recipe_m300x250_2": {
        "size": [
            [
                1,
                1
            ],
            [
                320,
                50
            ],
            [
                300,
                250
            ],
            [
                320,
                100
            ]
        ]
    },
    "ei_article_d970x250_1": {
        "size": [
            [
                970,
                250
            ],
            [
                728,
                90
            ],
            [
                970,
                90
            ]
        ]
    },
    "ei_article_d300x250_2": {
        "size": [
            [
                300,
                250
            ]
        ]
    },
    "ei_article_d728x90_3": {
        "size": [
            [
                728,
                90
            ]
        ]
    },
    "ei_article_d300x600_3": {
        "size": [
            [
                300,
                250
            ],
            [
                300,
                600
            ]
        ],
        refresh: 30000
    },
    "ei_article_d300x600_4": {
        "size": [
            [
                300,
                250
            ],
            [
                300,
                600
            ]
        ],
        refresh: 30000
    },
    "ei_article_d300x250_a_incontent": {
        "size": [
            [
                728,
                90
            ],
            [
                300,
                250
            ]
        ]
    },
    "ei_article_d300x250_b_incontent": {
        "size": [
            [
                728,
                90
            ],
            [
                300,
                250
            ]
        ]
    },
    "ei_article_d300x250_c_incontent": {
        "size": [
            [
                728,
                90
            ],
            [
                300,
                250
            ]
        ]
    },
    "ei_article_d300x250_d_incontent": {
        "size": [
            [
                728,
                90
            ],
            [
                300,
                250
            ]
        ]
    },
    "ei_article_d300x250_e_incontent": {
        "size": [
            [
                728,
                90
            ],
            [
                300,
                250
            ]
        ]
    },
    "ei_article_d300x250_f_incontent": {
        "size": [
            [
                728,
                90
            ],
            [
                300,
                250
            ]
        ]
    },
    "ei_article_m300x250_1": {
        "size": [
            [
                320,
                50
            ],
            [
                320,
                100
            ]
        ]
    },
    "ei_article_m300x250_a_incontent": {
        "size": [
            [
                320,
                100
            ],
            [
                320,
                50
            ],
            [
                300,
                250
            ]
        ]
    },
    "ei_article_m300x250_b_incontent": {
        "size": [
            [
                320,
                100
            ],
            [
                320,
                50
            ],
            [
                300,
                250
            ]
        ]
    },
    "ei_article_m300x250_c_incontent": {
        "size": [
            [
                320,
                100
            ],
            [
                320,
                50
            ],
            [
                300,
                250
            ]
        ]
    },
    "ei_article_m300x250_d_incontent": {
        "size": [
            [
                320,
                100
            ],
            [
                320,
                50
            ],
            [
                300,
                250
            ]
        ]
    },
    "ei_article_m300x250_e_incontent": {
        "size": [
            [
                320,
                100
            ],
            [
                320,
                50
            ],
            [
                300,
                250
            ]
        ]
    },
    "ei_article_m300x250_f_incontent": {
        "size": [
            [
                320,
                100
            ],
            [
                320,
                50
            ],
            [
                300,
                250
            ]
        ]
    },
    "ei_article_m320x50": {
        "size": [
            [
                320,
                50
            ],
            [
                1,
                1
            ],
            "fluid"
        ]
    },
    "ei_article_m300x250_2": {
        "size": [
            [
                320,
                50
            ],
            [
                300,
                250
            ],
            [
                320,
                100
            ]
        ]
    },
    "ei_video_d970x250_1": {
        "size": [
            [
                970,
                250
            ],
            [
                728,
                90
            ],
            [
                970,
                90
            ]
        ]
    },
    "ei_video_d728x90_3": {
        "size": [
            728,
            90
        ]
    },
    "ei_video_d300x600_3": {
        "size": [
            [
                300,
                250
            ],
            [
                300,
                600
            ]
        ]
    },
    "ei_video_m300x250_1": {
        "size": [
            [
                320,
                50
            ],
            [
                320,
                100
            ]
        ]
    },
    "ei_video_m300x250_2": {
        "size": [
            [
                320,
                50
            ],
            [
                300,
                250
            ],
            [
                320,
                100
            ]
        ]
    },
    "ei_category_d970x250_1": {
        "size": [
            [
                970,
                250
            ],
            [
                728,
                90
            ],
            [
                970,
                90
            ]
        ]
    },
    "ei_category_m300x250_1": {
        "size": [
            [
                320,
                50
            ],
            [
                320,
                100
            ]
        ]
    },
    "ei_gallery_d970x250_1": {
        "size": [
            [
                970,
                250
            ],
            [
                728,
                90
            ],
            [
                970,
                90
            ]
        ]
    },
    "ei_gallery_d728x90_3": {
        "size": [
            [
                728,
                90
            ]
        ]
    },
    "ei_gallery_d300x600_3": {
        "size": [
            [
                300,
                250
            ],
            [
                300,
                600
            ]
        ]
    },
    "ei_gallery_m300x250_1": {
        "size": [
            [
                320,
                50
            ],
            [
                320,
                100
            ]
        ]
    },
    "ei_gallery_m300x250_2": {
        "size": [
            [
                320,
                50
            ],
            [
                300,
                250
            ],
            [
                320,
                100
            ]
        ]
    }
}