class AdCounters{
    constructor(){
        this.adunitsCounters = {};
    }

    countAdUnit(adUnitName){
       
        if(Reflect.apply(Object.hasOwnProperty , this.adunitsCounters , [adUnitName])){
            this.adunitsCounters[adUnitName] = this.adunitsCounters[adUnitName] + 1;
            return this;
        }
        this.adunitsCounters[adUnitName] =1;
        return this;
    }

    getAdUnitCounter(adUnit){
        return this.adunitsCounters[adUnit];
    }


}

let adCounters = new AdCounters();
export  {adCounters};

