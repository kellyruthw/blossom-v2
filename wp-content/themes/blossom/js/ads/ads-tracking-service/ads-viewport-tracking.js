import {
    getScrollHandler,
    ViewPortManger,
    
} from "../../utils";
import { adPanelReportWithPrebidInfo } from ".";
import { SELECTOR_ROOT_LOADER } from "../../load-next-article";

export function registerViewportTracking(elem, placement) {
    
    
    let adViewportManager = new ViewPortManger("#" + elem.id, SELECTOR_ROOT_LOADER, true);
    adViewportManager.setCallbackInViewPort((element) => {
        if (!element.getAttribute(" ")) {

            adPanelReportWithPrebidInfo(placement , "adViewportOpen" );
            element.setAttribute("adinviewport", true);
        }

    });
    getScrollHandler().addCallback(function () {
        adViewportManager.run();
    });

}