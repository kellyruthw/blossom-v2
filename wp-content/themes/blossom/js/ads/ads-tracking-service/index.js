import {

    tempReportGooglePatch
} from "../../tracking-central/article-state";

import trackingBuilder from "../../tracking-central";


import {
    pixelCpmForFacebook,
    accumaltedCpm,
    pixelCmpForFacebookV2
} from "../cpm.manager";

import {
    cpmPerLineitem
} from "../cpm-perline-item";
import {
    getWinningPreBidder
} from "../prebid/prebid-winnings/prebid-winner";
import {
    getAdUnitNameFromAdUnitCode
} from "..";
import {
    fbqWatcher
} from "../../3rd-parties-objects-watchers";


let init = false;
let lockfbpix = false;
let userSeeAdCounter = 0;

export default function (googletag) {
    if (init) {
        return;
    }
    googletag.cmd.push(function () {


        googletag.pubads().addEventListener('slotOnload', function (event) {

            adPanelReportWithPrebidInfo(event, "adOpen", pixelCpmForFacebook);
        });


        googletag.pubads().addEventListener('slotRenderEnded', function (event) {

            // let {
            //     campaignId,
            //     advertiserId,
            //     companyIds,
            //     creativeId,
            //     creativeTemplateId,
            //     isBackfill,
            //     isEmpty,
            //     size,
            //     sourceAgnosticCreativeId,
            //     sourceAgnosticLineItemId
            // } = event;
            // let shadowEvt = {
            //     cid: campaignId,
            //     advid: advertiserId,
            //     comid: companyIds,
            //     crtvid: creativeId,
            //     crtvTmpid: creativeTemplateId,
            //     isbfil: isBackfill,
            //     isEmpty,
            //     size,
            //     sac: sourceAgnosticCreativeId,
            //     sal: sourceAgnosticLineItemId
            // };
            //let payload = JSON.stringify(shadowEvt);
            //payload = payload.replace(/null/g, "");
            //let payload1 = payload.substring(0, 98);
            //let payload2 = payload.substring(98, 196);
            let el = document.getElementById(event.slot.getSlotElementId());
            let adId = el.dataset.adId;
            let auctionId = el.dataset.auctionId;
            let adUnitName = getAdUnitNameFromAdUnitCode(el.id);

            // trackingBuilder("dfpResponse")
            //     .adunit(event.slot.placement.adUnit)
            //     .extras(payload1 ,payload2).send();

            if (event.lineItemId && cpmPerLineitem[event.lineItemId]) {
                let lineitemTracker = trackingBuilder("lineitemCpm")
                    .adunit(adUnitName)
                    .adId(adId)
                    .auctionId(auctionId)
                    .adUnitCounter(el.dataset.slotDefineCounter)
                    .extras(cpmPerLineitem[event.lineItemId])
                    .extras(event.lineItemId);
                if (event.slot.getTargetingMap() && event.slot.getTargetingMap().hb_bidder &&
                    event.slot.getTargetingMap().hb_bidder.length && event.slot.getTargetingMap().hb_bidder.length > 0) {
                    lineitemTracker.extras(event.slot.getTargetingMap().hb_bidder[0]);
                }
                //lineitemTracker.send();


            }

            if (!event.isEmpty) {
                //adPanelReportWithPrebidInfo(event.slot.placement, "adPotential");

            }
            let targetingBidder = null;
            if (event.slot.getTargetingMap() && event.slot.getTargetingMap().hb_bidder &&
                event.slot.getTargetingMap().hb_bidder.length && event.slot.getTargetingMap().hb_bidder.length > 0) {
                targetingBidder = event.slot.getTargetingMap().hb_bidder[0];

                let prebidWinnder = getWinningPreBidder(event);
                let revenue = null;
                if (event.isEmpty) {


                    if (prebidWinnder) {
                        revenue = prebidWinnder.cpm;
                    }
                    trackingBuilder("prebidButNoAd")
                        .adUnitCounter(el.dataset.slotDefineCounter).adId(adId)
                        .auctionId(auctionId).adunit(adUnitName).revenue(revenue).extras(targetingBidder).send();
                    return;
                }

                let winnerBidderCode = event.advertiserId;
                if (4641427377 == winnerBidderCode) {
                    if (prebidWinnder) {
                        revenue = prebidWinnder.cpm;
                    }
                    //trackingBuilder("prebidButCookingBook").adunit(event.slot.placement.adUnit).revenue(revenue).extras(targetingBidder).send();
                }

            }

        });

        googletag.pubads().addEventListener('impressionViewable', function (event) {

            userSeeAdCounter = userSeeAdCounter + 1;
            window.fbq('trackCustom', 'userSeeAd', {
                "value": userSeeAdCounter
            });
            adPanelReportWithPrebidInfo(event, "adImpressionViewable", pixelCmpForFacebookV2);



        });


    });
    init = true;


}

export function adPanelReport(eventName, extraData) {
    tempReportGooglePatch(eventName);
    trackingBuilder(eventName).extras(extraData).send();



}

export function adPanelReportWithPrebidInfo(event, eventName, pixelToFacebookCallback = null) {

    let tracker = trackingBuilder(eventName);
    let el = document.getElementById(event.slot.getSlotElementId());
    let adUnitName = getAdUnitNameFromAdUnitCode(el.id);
    let prebidWinnder = getWinningPreBidder(event);
    if (prebidWinnder) {
        tracker.bidder(prebidWinnder.bidderCode)
            .revenue(prebidWinnder.cpm)
            .adunit(adUnitName);

    }

    if (pixelToFacebookCallback && prebidWinnder) {
        let {
            cpm
        } = prebidWinnder;
        pixelToFacebookCallback({
            cpm,
            fbqFunc: window.fbq
        });

    }

    let adId = el.dataset.adId
    let auctionId = el.dataset.auctionId;
    tracker.auctionId(auctionId)
        .adId(adId)
        .adUnitCounter(el.dataset.slotDefineCounter)
        .extras(accumaltedCpm).send();
    if (prebidWinnder && !lockfbpix) {
        fbqWatcher.push(() => {
            window.fbq('trackCustom', 'firstAdCpmFb', {
                "cpm": prebidWinnder.cpm
            });

        });

        lockfbpix = true;
    }

}