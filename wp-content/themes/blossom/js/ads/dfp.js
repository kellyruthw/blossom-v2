import {
    getAdUnitNameFromAdUnitCode
} from '.';

import {
    dfpSettings
} from './ads-settings';

import {
    getUrlParams
} from '../utils';
import {
    isSponsored
} from '../page-types';
import {
    origArticle
} from '../tracking-central/article-state';
import {
    Md5
} from 'ts-md5';
import {
    getUserLast2digit
} from '../user-manager';

export const dfpAccountId = '2699062';

class AdCounter {
    static counter = 0;

    static hit() {
        this.counter = this.counter + 1;
    }


}

const targetingKeys = {
    sponsored: 'sponsored',
    originArticle: 'originArticle',
    articleId: 'articleId',
    randomNumber: 'random_number',
    dmAdId: 'dmAdId',
    dmCampaignId: 'dmCampaignId',
    dmAdsetId: 'dmAdsetId',
    utmCampaignFmt: "utm_campaign_fmt",
    adDisplayed: 'ad_displayed',
    refreshCounter: 'refresh_counter',
    provider: 'provider',
    utmCampaign: 'utm_campaign',
    utmMedium: 'utm_medium',
    utmContent: 'utm_content',
    utmSource: 'utm_source'



}
export class DfpManager {
    constructor(widnow, adWrapper) {
        this.adWrapper = adWrapper;
        this.window = window;
        this.adUnitCode = adWrapper.id;



    }



    defineSlot() {

        this.window.googletag.cmd.push(() => {

            let adUnitName = getAdUnitNameFromAdUnitCode(this.adUnitCode);
            let slot = this.window.googletag.defineSlot(`/${dfpAccountId}/${adUnitName}`, dfpSettings[adUnitName].size, this.adUnitCode).addService(window.googletag.pubads());
            this.slot = slot;
            AdCounter.hit();
            return slot;

        });


    }

    setTargeting() {
        this.window.googletag.cmd.push(() => {
            let {
                googletag
            } = this.window;
            let urlParams = getUrlParams();
            googletag.pubads().setTargeting(targetingKeys.utmCampaign, urlParams[targetingKeys.utmCampaign]);
            googletag.pubads().setTargeting(targetingKeys.utmContent, urlParams[targetingKeys.utmContent]);
            googletag.pubads().setTargeting(targetingKeys.utmSource, urlParams[targetingKeys.utmSource]);
            googletag.pubads().setTargeting(targetingKeys.utmMedium, urlParams[targetingKeys.utmMedium]);
            googletag.pubads().setTargeting(targetingKeys.sponsored, isSponsored ? 'true' : 'false');
            googletag.pubads().setTargeting(targetingKeys.originArticle, origArticle.substring(0, 20));
            googletag.pubads().setTargeting(targetingKeys.articleId, Md5.hashStr(origArticle));
            googletag.pubads().setTargeting(targetingKeys.randomNumber, getUserLast2digit());
            googletag.pubads().setTargeting(targetingKeys.dmAdId, urlParams.utm_content);
            googletag.pubads().setTargeting(targetingKeys.dmCampaignId, urlParams.utm_medium);
            googletag.pubads().setTargeting(targetingKeys.dmAdsetId, urlParams.utm_campaign);
            if (urlParams.utm_campaign && urlParams.utm_campaign.includes("pps")) {
                googletag.pubads().setTargeting(targetingKeys.provider, "pubplus");
            }
            if (urlParams.utm_campaign) {
                googletag.pubads().setTargeting(targetingKeys.utmCampaignFmt, Md5.hashStr("'" + decodeURIComponent(urlParams.utm_campaign) + "'"));
            }

            this.slot.addService(googletag.pubads()).setTargeting(targetingKeys.adDisplayed, AdCounter.counter);
            this.slot.addService(googletag.pubads()).setTargeting(targetingKeys.refreshCounter, this.adWrapper.dataset.slotDefineCounter);

        });



    }





    countNumOfTimesSlotDefined() {

        if (!this.adWrapper.dataset.slotDefineCounter) {
            this.adWrapper.dataset.slotDefineCounter = 0;
            return;
        }
        let counter = parseInt(this.adWrapper.dataset.slotDefineCounter);
        counter = counter + 1;
        this.adWrapper.dataset.slotDefineCounter = counter;

    }

    displaySlot() {
        this.window.googletag.cmd.push(() => {
            this.window.googletag.enableServices();
            this.window.googletag.display(this.slot);
            this.window.googletag.pubads().refresh([this.slot]);

        });
    }
}