export const getAdUnitNameFromAdUnitCode = (wrapperId) => {
    let adUnitName = wrapperId.replace(/gpt_\d*_/, "");
    return adUnitName;

}



import {
    ViewPortManger,
    getScrollHandler,
    guid,

} from '../utils2';
import jQuery from 'jquery';
import {
    DfpManager
} from './dfp';
import {
    HeaderBidding
} from './prebid/header-bidding';
import {
    dfpSettings
} from './ads-settings';
import initAdTracking from "./ads-tracking-service/index";
import trackingBuilder from "../tracking-central/index";
import {
    isMobile
} from 'is-mobile';
import {
    isArticlePage
} from '../page-types';
import { SELECTOR_ROOT_LOADER } from '../load-next-article';


const CSS_SELECTOR_ADS_WRAPPER = ".ad-place-holder";
const SUBHEADER_SELECTOR = ".single .entry-content h3 , .single .entry-content h2";
const stickyAdTemplate = '<div class="ad-place-holder sticky-mobile-bottom" id="gpt_0_ei_article_m320x50"/>';
let runStaticAdsAlready = false;


function getInlineAdTemplate(){
    let platform = isMobile()?"m":"d";
    return `<div id='gpt_${window.parseInt(Math.random()*100000)}_ei_article_${platform}300x250_a_incontent' class='ad-place-holder'></div>`;

}



function injectArticleInlineAdsIfNeeded() {

    let indexAddition = 0;
    
    if (!isArticlePage) {
        return;
    }
    [...document.querySelectorAll(SUBHEADER_SELECTOR)].forEach((elem, index) => {
        if (!elem.dataset.inlineAdWrapperInit) {
            let elQuery = jQuery(elem);
            elQuery.before(getInlineAdTemplate());
            elem.dataset.inlineAdWrapperInit = true;
            if (index > 9 && isMobile()) {
                let nextElem1 = elQuery.next();
                let nextElem2 = nextElem1.next();
                let forbiddenNext = ['H3', 'H2'];
                console.log("for elQuery ", elQuery.prop("tagName"), " nextElem1  ", nextElem1.prop("tagName"), ", nextElem2  ", nextElem2.prop("tagName"));
                if (!forbiddenNext.includes(nextElem1.prop("tagName")) && !forbiddenNext.includes(nextElem2.prop("tagName"))) {
                    indexAddition = indexAddition +1
                    
                    nextElem2.before(getInlineAdTemplate());
                    
                }
            }

        }
    });

    if (isMobile() && isArticlePage) {
        addEventListener('scroll', () => {

            if (!runStaticAdsAlready) {
                let indexViewPort = window.parseInt(scrollY / document.documentElement.clientHeight);
                if (indexViewPort > 1) {
                    jQuery(SELECTOR_ROOT_LOADER).append(stickyAdTemplate);
                    runStaticAdsAlready = true;
                }
            }
        });
    }
}







export class AdManager {
    constructor(window) {
        this.window = window;
        
        this.adDisplayedCounter = 0;
        this.init();
    }

    init = () => {

        initAdTracking(this.window.googletag, this.window.ga);
        injectArticleInlineAdsIfNeeded();
        this.adsViewPortManager = new ViewPortManger(CSS_SELECTOR_ADS_WRAPPER,
            SELECTOR_ROOT_LOADER,
            true,
            this.window.innerHeight * 2.5, injectArticleInlineAdsIfNeeded);
        this.adsViewPortManager.setCallbackInViewPort((adWrapper) => {
            new AdLoader(this.window, adWrapper).loadAd()

        });
        this.adsViewPortManager.run();
        getScrollHandler().addCallback(() => {
            this.adsViewPortManager.run();
        });

    }





}


class AdLoader {
    constructor(window, adWrapper) {
        this.window = window;
        this.adWrapper = adWrapper;
        this.adWrapper.dataset.adId = guid();
        this.adUnitName = getAdUnitNameFromAdUnitCode(this.adWrapper.id);
        adWrapper.dataset.AdUnit = this.adUnitName;


    }



    loadAd = () => {

        trackingBuilder("adStart").adunit(this.adUnitName).adId(this.adWrapper.dataset.adId).send();

        if (this.adWrapper.dataset.adAlreadyLoad) {
            return;
        }

        this.window.pbjs && (this.window.pbjs.initAdserverSet = false);
        this.adWrapper.dataset.adAlreadyLoad = true;
        this.runManagers();
        this.runRefreshAdIfNeeded()


    }

    runRefreshAdIfNeeded() {
        let adUnitName = getAdUnitNameFromAdUnitCode(this.adWrapper.id);
        let {
            refresh
        } = dfpSettings[adUnitName];
        if (refresh) {
            setTimeout(() => {
                this.adWrapper.dataset.adAlreadyLoad = "";
                this.loadAd();
            }, refresh);
        }

    }



    // eslint-disable-next-line require-await
    runManagers = async () => {
        if (!this.dfpManager) {
            this.dfpManager = new DfpManager(this.window, this.adWrapper);
            this.dfpManager.defineSlot();
        }
        this.dfpManager.countNumOfTimesSlotDefined();
        this.dfpManager.setTargeting();
        this.headerBidder = new HeaderBidding(this.window, this.adWrapper);
        this.headerBidder.runBidders();
        this.dfpManager.displaySlot();



    }


}