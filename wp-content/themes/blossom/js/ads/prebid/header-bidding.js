/* eslint-disable array-bracket-newline */
import {
    dfpAccountId
} from "../dfp";
import {
    getAdUnitNameFromAdUnitCode
} from "..";
import {
    getPrebidBiddersCustomSettings,
    getHeaderBiddingForAdUnitCode
} from "./prebid-settings-covertors";
import {
    dfpSettings
} from "../ads-settings";
var isMobile = require('is-mobile');


const PREBID_TIMEOUT = 2400;
const amazonBidderSetting = {
    pubID: 'ce85af06-a81d-4ad0-a997-c724c7c3b973',
    adServer: 'googletag',
};

const AMAZON_AUCTION_PLACE = 'a9';
const PREBID_AUCTION_PLACE = 'prebid'

export class HeaderBidding {
    constructor(widnow, adWrapper) {
        this.window = window;
        this.adWrapper = adWrapper;
        this.adUnitCode = adWrapper.id;
        this.adUnitName = getAdUnitNameFromAdUnitCode(this.adUnitCode)
        this.account = dfpAccountId;
        this.requestManager = {
            adserverRequestSent: false,
        };
        this.bidders = [AMAZON_AUCTION_PLACE, PREBID_AUCTION_PLACE];
        this.bidders.forEach((bidder) => {
            this.requestManager[bidder] = false;
        });
        this.promise = new Promise((resolve) => {
            resolve();
        })


    }

    runBidders = () => {
        this.window.setTimeout(() => {
            this.sendAdserverRequest();
        }, PREBID_TIMEOUT);
        this.window.pbjs.que = this.window.pbjs.que || [];
        this.window.apstag.init(amazonBidderSetting);
        let apstagSlots = [{
            slotID: this.adUnitCode,
            slotName: this.account + "/" + this.adUnitName,
            sizes: dfpSettings[this.adUnitName].size
        }];



        this.window.pbjs.que.push(() => {
            let minFloor = isMobile() ? 0.3 : 0.7;
            minFloor = parseFloat(minFloor);
            this.window.pbjs.setConfig({
                priceGranularity: {
                    "buckets": [{
                            "min": minFloor,
                            "max": 6,
                            "increment": 0.01
                        },
                        {
                            "min": 6,
                            "max": 16,
                            "increment": 0.05
                        },
                        {
                            "min": 16,
                            "max": 25,
                            "increment": 0.5
                        }
                    ]
                },

            });
            let adUnits = getHeaderBiddingForAdUnitCode(this.adUnitCode);
            this.window.pbjs.addAdUnits(adUnits);
            this.window.pbjs.bidderSettings = {
                ...this.window.pbjs.bidderSettings,
                ...getPrebidBiddersCustomSettings()
            };
            this.window.pbjs.requestBids({
                bidsBackHandler: () => {
                    this.headerBidderBack(PREBID_AUCTION_PLACE);
                }
            });
            this.window.apstag.fetchBids({
                slots: apstagSlots,
                timeout: PREBID_TIMEOUT
            }, () => {
                this.headerBidderBack(AMAZON_AUCTION_PLACE);
            });
        });

        this.window.googletag.cmd.push(() => {
            if (!this.window.googletag.isInitDisabled) {
                this.window.googletag.pubads().disableInitialLoad();
                this.window.googletag.pubads().collapseEmptyDivs(true);
            }
            this.window.googletag.isInitDisabled = true;

        });


    };

    headerBidderBack = (bidder) => {
        // return early if request to adserver is already sent
        if (this.requestManager.adserverRequestSent === true) {
            return;
        }
        // flip bidder back flag
        if (bidder === AMAZON_AUCTION_PLACE) {

            this.requestManager.a9 = true;

        } else if (bidder === PREBID_AUCTION_PLACE) {

            this.requestManager.prebid = true;
        }

        // if all bidders are back, send the request to the ad server
        if (this.allBiddersBack()) {
            this.sendAdserverRequest();
        }
    }

    sendAdserverRequest = () => {


        // return early if request already sent
        if (this.requestManager.adserverRequestSent === true) {
            return;
        }

        // flip the boolean that keeps track of whether the adserver request was sent
        this.requestManager.adserverRequestSent = true;
        // flipthis.window.pbjs boolean to signal tothis.window.pbjs the ad server has already been called
        this.window.pbjs.adserverRequestSent = true;

        // flip boolean for adserver request to avoid duplicate requests
        this.requestManager.sendAdserverRequest = true;

        // set bid targeting and make ad request to GAM
        this.window.googletag.cmd.push(async () => {
            this.window.apstag.setDisplayBids();
            this.window.pbjs.setTargetingForGPTAsync();
            await this.signalAllBiddersBack();


        });
    }

    allBiddersBack = () => {

        let tempArr = this.bidders.map((bidder) => {
            return this.requestManager[bidder];
        });
        tempArr = tempArr.filter(Boolean)
        let flag = tempArr.length === this.bidders.length;
        console.log("this is flag :  ", flag);
        return flag;
    }

    signalAllBiddersBack = async () => {
        await this.promise;
    }









}