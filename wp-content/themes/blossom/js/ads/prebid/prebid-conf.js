/* eslint-disable max-lines */

export const prebids = {
    "aol": {
        "common": {
            "network": "11553.1",

        },
        "ei_recipe_d970x250_1": {
            "placement": ["4946499", "4946486"]
        },
        "ei_recipe_d728x90_3": {
            "placement": ["4946490"]
        },
        "ei_recipe_d300x600_3": {
            "placement": ["4946501", "4946507"]
        },

        "ei_recipe_m300x250_1": {
            "placement": ["4946514", "4946535"]
        },
        "ei_recipe_m300x250_2": {
            "placement": ["4946513", "4946522", "4946536"]
        },

        "ei_article_d970x250_1": {
            "placement": ["4946477", "4946494"]
        },
        "ei_article_d728x90_3": {
            "placement": ["4946487"]
        },
        "ei_article_d300x600_3": {
            "placement": ["4946481"]
        },
        "ei_article_d300x600_4": {
            "placement": [""]
        },
        "ei_article_d300x250_a_incontent": {
            "placement": ["4946482", "4946498"]
        },
        "ei_article_d300x250_b_incontent": {
            "placement": ["4946496", "4946509"]
        },
        "ei_article_d300x250_c_incontent": {
            "placement": ["4946478", "4946480"]
        },
        "ei_article_d300x250_d_incontent": {
            "placement": ["4946485", "4946493"]
        },
        "ei_article_d300x250_e_incontent": {
            "placement": ["4946510", "4946504"]
        },
        "ei_article_d300x250_f_incontent": {
            "placement": ["4946505", "4946484"]
        },
        "ei_article_m320x50": {
            "placement": ["4946000"]
        },
        "ei_article_m300x250_1": {
            "placement": ["4946545", "4946520"]
        },
        "ei_article_m300x250_a_incontent": {
            "placement": ["4946519", "4946541", "4946548"]
        },
        "ei_article_m300x250_b_incontent": {
            "placement": ["4946529", "4946527", "4946526"]
        },
        "ei_article_m300x250_c_incontent": {
            "placement": ["4946551", "4946544", "4946521"]
        },
        "ei_article_m300x250_d_incontent": {
            "placement": ["4946540", "4946528", "4946516"]
        },
        "ei_article_m300x250_e_incontent": {
            "placement": ["4946518", "4946533", "4946547"]
        },
        "ei_article_m300x250_f_incontent": {
            "placement": ["4946542", "4946534", "4946512"]
        }

    },
    "sharethrough": {
        "ei_recipe_d970x250_1": {
            "pkey": "zP5NthoeEmV1JTUKCR2nwscv"
        },
        "ei_recipe_d728x90_3": {
            "pkey": "2HMrjmNynHSSSxso12mN5mfj"
        },
        "ei_recipe_d300x600_3": {
            "pkey": "4pQh8VpgekD588iV8xyin3Co"

        },

        "ei_recipe_m300x250_1": {
            "pkey": "CBZiRfMURUgbYqV9heER81Hn"
        },
        "ei_recipe_m300x250_2": {
            "pkey": "dWC13wrFvAKGgBztSjZjJVev"
        },

        "ei_article_d970x250_1": {
            "pkey": "24Vd5p1nBds1GfzVKiw3cYiz"
        },
        "ei_article_d728x90_3": {
            "pkey": "XJyZrha7Vc2DN5WHWhU986Zw"
        },
        "ei_article_d300x600_3": {
            "pkey": "c5Q6YHvT5K968SG6FHKpa3sZ"
        },
        "ei_article_d300x600_4": {
            "pkey": "P5WwBzheGTFRxarYm4LuR8NW"
        },
        //"ei_article_d300x600_4":{
        //   "pkey": "_article_d300x600_4"
        //},
        "ei_article_d300x250_a_incontent": {
            "pkey": "mXqkoxKN9CFBCYGAv2Kdpjmi"
        },
        "ei_article_d300x250_b_incontent": {
            "pkey": "fwGxjUtd4wsPAqjDbcyqYvKm"
        },
        "ei_article_d300x250_c_incontent": {
            "pkey": "9aeheaUfGyPVp5aZXNndiEJB"
        },
        "ei_article_d300x250_d_incontent": {
            "pkey": "DB5Esv2jXY7ocogadhTcGXCr"
        },
        "ei_article_d300x250_e_incontent": {
            "pkey": "5iSfXoYsZk6owU41B2raCKQB"
        },
        "ei_article_d300x250_f_incontent": {
            "pkey": "Dsi1u1CTBTfW1mA8668TWB5t"
        },


        "ei_article_m320x50": {
            "pkey": "zVkhCMpAvYiMbh7byiE7eJaa"
        },
        "ei_article_m300x250_1": {
            "pkey": "zVkhCMpAvYiMbh7byiE7eJiU"
        },
        "ei_article_m300x250_a_incontent": {
            "pkey": "siYL7jDdAeMvNcSKReVw67Q9"
        },
        "ei_article_m300x250_b_incontent": {
            "pkey": "hEp3dob2rCJmFBodYPiXxUya"
        },
        "ei_article_m300x250_c_incontent": {
            "pkey": "M24sBCGfw64C6BdLS1HMdAui"
        },
        "ei_article_m300x250_d_incontent": {
            "pkey": "ZncfWNffYSaoonHjj356wD6A"
        },
        "ei_article_m300x250_e_incontent": {
            "pkey": "XLoGw5ryPRVNCffvUPaugf5E"
        },
        "ei_article_m300x250_f_incontent": {
            "pkey": "3QzHS3CCaKnTVFyHnp7Q6Wp9"
        }

    },
    "sonobi": {
        "ei_recipe_d970x250_1": {
            "ad_unit": "/2699062/ei_recipe_d970x250_1"
        },
        "ei_recipe_d728x90_3": {
            "ad_unit": "/2699062/ei_recipe_d728x90_3"
        },
        "ei_recipe_d300x600_3": {
            "ad_unit": "/2699062/ei_recipe_d300x600_3"

        },

        "ei_recipe_m300x250_1": {
            "ad_unit": "/2699062/ei_recipe_m300x250_1"
        },
        "ei_recipe_m300x250_2": {
            "ad_unit": "/2699062/ei_recipe_m300x250_2"
        },

        "ei_article_d970x250_1": {
            "ad_unit": "/2699062/ei_article_d970x250_1"
        },
        "ei_article_d728x90_3": {
            "ad_unit": "/2699062/ei_article_d728x90_3"
        },
        "ei_article_d300x600_3": {
            "ad_unit": "/2699062/ei_article_d300x600_3"
        },
        "ei_article_d300x600_4": {
            "ad_unit": "/2699062/ei_article_d300x600_4"
        },
        //"ei_article_d300x600_4":{
        //   "ad_unit": "/2699062/ei_article_d300x600_4"
        //},
        "ei_article_d300x250_a_incontent": {
            "ad_unit": "/2699062/ei_recipe_d970x250_1"
        },
        "ei_article_d300x250_b_incontent": {
            "ad_unit": "/2699062/ei_article_d300x250_b_incontent"
        },
        "ei_article_d300x250_c_incontent": {
            "ad_unit": "/2699062/ei_article_d300x250_c_incontent"
        },
        "ei_article_d300x250_d_incontent": {
            "ad_unit": "/2699062/ei_article_d300x250_d_incontent"
        },
        "ei_article_d300x250_e_incontent": {
            "ad_unit": "/2699062/ei_article_d300x250_e_incontent"
        },
        "ei_article_d300x250_f_incontent": {
            "ad_unit": "/2699062/ei_article_d300x250_f_incontent"
        },
        "ei_article_m320x50": {
            "ad_unit": "/2699062/ei_article_m320x50"
        },
        "ei_article_m300x250_1": {
            "ad_unit": "/2699062/ei_article_m300x250_1"
        },
        "ei_article_m300x250_a_incontent": {
            "ad_unit": "/2699062/ei_article_m300x250_a_incontent"
        },
        "ei_article_m300x250_b_incontent": {
            "ad_unit": "/2699062/ei_article_m300x250_b_incontent"
        },
        "ei_article_m300x250_c_incontent": {
            "ad_unit": "/2699062/ei_article_m300x250_c_incontent"
        },
        "ei_article_m300x250_d_incontent": {
            "ad_unit": "/2699062/ei_article_m300x250_d_incontent"
        },
        "ei_article_m300x250_e_incontent": {
            "ad_unit": "/2699062/ei_article_m300x250_e_incontent"
        },
        "ei_article_m300x250_f_incontent": {
            "ad_unit": "/2699062/ei_article_m300x250_f_incontent"
        },



    },

    "districtm": {
        "bidderSettings": {
            "bidCpmAdjustment": 0.7,
        },
        "ei_recipe_d970x250_1": {
            "placementId": "14335999"
        },
        "ei_recipe_d728x90_3": {
            "placementId": "14335998"
        },
        "ei_recipe_d300x600_3": {
            "placementId": "14335997"
        },
        "ei_recipe_m300x250_1": {
            "placementId": "14336000"
        },
        "ei_recipe_m300x250_2": {
            "placementId": "14336002"
        },

        "ei_article_d970x250_1": {
            "placementId": "14335979"
        },
        "ei_article_d728x90_3": {
            "placementId": "14335978"
        },
        "ei_article_d300x600_3": {
            "placementId": "14335976"
        },
        "ei_article_d300x600_4": {
            "placementId": "14335977"
        },
        "ei_article_d300x250_a_incontent": {
            "placementId": "14335970"
        },
        "ei_article_d300x250_b_incontent": {
            "placementId": "14335971"
        },
        "ei_article_d300x250_c_incontent": {
            "placementId": "14335972"
        },
        "ei_article_d300x250_d_incontent": {
            "placementId": "14335973"
        },
        "ei_article_d300x250_e_incontent": {
            "placementId": "14335974"
        },
        "ei_article_d300x250_f_incontent": {
            "placementId": "14335975"
        },
        "ei_article_m320x50": {
            "placementId": "14335000"
        },
        "ei_article_m300x250_1": {
            "placementId": "14335980"
        },
        "ei_article_m300x250_a_incontent": {
            "placementId": "14335982"
        },
        "ei_article_m300x250_b_incontent": {
            "placementId": "14335983"
        },
        "ei_article_m300x250_c_incontent": {
            "placementId": "14335985"
        },
        "ei_article_m300x250_d_incontent": {
            "placementId": "14335986"
        },
        "ei_article_m300x250_e_incontent": {
            "placementId": "14335987"
        },
        "ei_article_m300x250_f_incontent": {
            "placementId": "14335988"
        },

        "ei_category_d970x250_1": {
            "placementId": "14335989"
        },

        "ei_category_m300x250_1": {
            "placementId": "14335990"
        },
    },
    "districtmDMX": {
        "bidderSettings": {
            "bidCpmAdjustment": 0.7,
        },
        "common": {
            "memberid": "101689"
        },


        "ei_recipe_d970x250_1": {
            "dmxid": ["263649"]
        },
        "ei_recipe_d728x90_3": {
            "dmxid": ["263648"]
        },
        "ei_recipe_d300x600_3": {
            "dmxid": ["263647"]
        },



        "ei_recipe_m300x250_1": {
            "dmxid": ["263650"]
        },
        "ei_recipe_m300x250_2": {
            "dmxid": ["263651"]
        },




        "ei_article_d970x250_1": {
            "dmxid": ["263631"]
        },
        "ei_article_d728x90_3": {
            "dmxid": ["263630"]
        },
        "ei_article_d300x600_3": {
            "dmxid": ["263628"]
        },
        "ei_article_d300x600_4": {
            "dmxid": ["263629"]
        },
        "ei_article_d300x250_a_incontent": {
            "dmxid": ["263622"]
        },
        "ei_article_d300x250_b_incontent": {
            "dmxid": ["263623"]
        },
        "ei_article_d300x250_c_incontent": {
            "dmxid": ["263624"]
        },
        "ei_article_d300x250_d_incontent": {
            "dmxid": ["263625"]
        },
        "ei_article_d300x250_e_incontent": {
            "dmxid": ["263626"]
        },
        "ei_article_d300x250_f_incontent": {
            "dmxid": ["263627"]
        },


        "ei_article_m320x50": {
            "dmxid": ["263521"]
        },
        "ei_article_m300x250_1": {
            "dmxid": ["263632"]
        },
        "ei_article_m300x250_a_incontent": {
            "dmxid": ["263634"]
        },
        "ei_article_m300x250_b_incontent": {
            "dmxid": ["263635"]
        },
        "ei_article_m300x250_c_incontent": {
            "dmxid": ["263636"]
        },
        "ei_article_m300x250_d_incontent": {
            "dmxid": ["263637"]
        },
        "ei_article_m300x250_e_incontent": {
            "dmxid": ["263638"]
        },
        "ei_article_m300x250_f_incontent": {
            "dmxid": ["263639"]

        },




        "ei_category_d970x250_1": {
            "dmxid": ["263640"]
        },



        "ei_category_m300x250_1": {
            "dmxid": ["263641"]

        },


    },
    "appnexus": {
        "ei_article_d970x250_1": {
            "placementId": "14459813"
        },
        "ei_article_d728x90_3": {
            "placementId": "14459812"
        },
        "ei_article_d300x600_3": {
            "placementId": "14459809"
        },
        "ei_article_d300x600_4": {
            "placementId": "14459810"
        },
        "ei_article_d300x250_a_incontent": {
            "placementId": "14459800"
        },
        "ei_article_d300x250_b_incontent": {
            "placementId": "14459802"
        },
        "ei_article_d300x250_c_incontent": {
            "placementId": "14459803"
        },
        "ei_article_d300x250_d_incontent": {
            "placementId": "14459804"
        },
        "ei_article_d300x250_e_incontent": {
            "placementId": "14459806"
        },
        "ei_article_d300x250_f_incontent": {
            "placementId": "14459807"
        },


        "ei_article_m320x50": {
            "placementId": "14735825"
        },
        "ei_article_m300x250_1": {
            "placementId": "14459814"
        },
        "ei_article_m300x250_a_incontent": {
            "placementId": "14459816"
        },
        "ei_article_m300x250_b_incontent": {
            "placementId": "14459817"
        },
        "ei_article_m300x250_c_incontent": {
            "placementId": "14459818"
        },
        "ei_article_m300x250_d_incontent": {
            "placementId": "14459819"
        },
        "ei_article_m300x250_e_incontent": {
            "placementId": "14459820"
        },
        "ei_article_m300x250_f_incontent": {
            "placementId": "14459821"

        }


    },
    "triplelift": {


        "ei_article_d970x250_1": {
            "inventoryCode": "FM_RON_HDX"
        },
        "ei_article_d728x90_3": {
            "inventoryCode": "FM_RON_HDX"
        },
        "ei_article_d300x600_3": {
            "inventoryCode": "FM_RON_HDX"
        },
        "ei_article_d300x600_4": {
            "inventoryCode": "FM_RON_HDX"
        },
        "ei_article_d300x250_a_incontent": {
            "inventoryCode": "FM_RON_HDX"
        },
        "ei_article_d300x250_b_incontent": {
            "inventoryCode": "FM_RON_HDX"
        },
        "ei_article_d300x250_c_incontent": {
            "inventoryCode": "FM_RON_HDX"
        },
        "ei_article_d300x250_d_incontent": {
            "inventoryCode": "FM_RON_HDX"
        },
        "ei_article_d300x250_e_incontent": {
            "inventoryCode": "FM_RON_HDX"
        },
        "ei_article_d300x250_f_incontent": {
            "inventoryCode": "FM_RON_HDX"
        },


        "ei_article_m320x50": {
            "inventoryCode": "FM_RON_HDX"
        },
        "ei_article_m300x250_1": {
            "inventoryCode": "FM_RON_HDX"
        },
        "ei_article_m300x250_a_incontent": {
            "inventoryCode": "FM_RON_HDX"
        },
        "ei_article_m300x250_b_incontent": {
            "inventoryCode": "FM_RON_HDX"
        },
        "ei_article_m300x250_c_incontent": {
            "inventoryCode": "FM_RON_HDX"
        },
        "ei_article_m300x250_d_incontent": {
            "inventoryCode": "FM_RON_HDX"
        },
        "ei_article_m300x250_e_incontent": {
            "inventoryCode": "FM_RON_HDX"
        },
        "ei_article_m300x250_f_incontent": {
            "inventoryCode": "FM_RON_HDX"
        },


    },
    "gumgum": {


        "ei_article_d970x250_1": {
            "inSlot": '25437'
        },
        //            "ei_article_d728x90_3": {
        //                "inSlot": '24553'
        //            },
        "ei_article_d300x600_3": {
            "inSlot": '25244'
        },
        "ei_article_d300x600_4": {
            "inSlot": '25245'
        },
        "ei_article_d300x250_a_incontent": {
            "inSlot": '24558'
        },
        "ei_article_d300x250_b_incontent": {
            "inSlot": '24559'
        },
        "ei_article_d300x250_c_incontent": {
            "inSlot": '24560'
        },
        "ei_article_d300x250_d_incontent": {
            "inSlot": '24561'
        },
        "ei_article_d300x250_e_incontent": {
            "inSlot": '24562'
        },
        "ei_article_d300x250_f_incontent": {
            "inSlot": '25243'
        },


        "ei_article_m320x50": {
            "inSlot": '25253'
        },
        "ei_article_m300x250_1": {
            "inSlot": '25246'
        },
        "ei_article_m300x250_a_incontent": {
            "inSlot": '25247'
        },
        "ei_article_m300x250_b_incontent": {
            "inSlot": '25248'
        },
        "ei_article_m300x250_c_incontent": {
            "inSlot": '25249'
        },
        "ei_article_m300x250_d_incontent": {
            "inSlot": '25250'
        },
        "ei_article_m300x250_e_incontent": {
            "inSlot": '25251'
        },
        "ei_article_m300x250_f_incontent": {
            "inSlot": '25252'
        },




        "ei_recipe_d970x250_1": {
            "inSlot": '25440'
        },
        "ei_recipe_d728x90_3": {
            "inSlot": '25256'
        },
        "ei_recipe_d300x600_3": {
            "inSlot": '25255'
        },



        "ei_recipe_m300x250_1": {
            "inSlot": '25257'
        },
        "ei_recipe_m300x250_2": {
            "inSlot": '25258'
        },


    },
    "ix": {
        "ei_article_d970x250_1": {
            'siteId': '349295',
            'size': [
                [728, 90],
                [970, 90],
                [970, 250]
            ]
        },
        "ei_article_d300x600_3": {
            'siteId': '349292',
            'size': [
                [300, 250],
                [300, 600]
            ]
        },
        "ei_article_d300x600_4": {
            'siteId': '349293',
            'size': [
                [300, 250],
                [300, 600]
            ]
        },
        "ei_article_d300x250_a_incontent": {
            'siteId': '349286',
            'size': [
                [300, 250],
                [728, 90]
            ]
        },
        "ei_article_d300x250_b_incontent": {
            'siteId': '349287',
            'size': [
                [
                    [300, 250],
                    [728, 90]
                ]
            ]
        },
        "ei_article_d300x250_c_incontent": {
            'siteId': '349288',
            'size': [
                [300, 250],
                [728, 90]
            ]
        },
        "ei_article_d300x250_d_incontent": {
            'siteId': '349289',
            'size': [
                [300, 250],
                [728, 90]
            ]
        },
        "ei_article_d300x250_e_incontent": {
            'siteId': '349290',
            'size': [
                [300, 250],
                [728, 90]
            ]
        },
        "ei_article_d300x250_f_incontent": {
            'siteId': '349291',
            'size': [
                [728, 90],
                [300, 250]
            ]
        },


        "ei_article_m320x50": {
            'siteId': '349297',
            'size': [
                [320, 50],
                [300, 250],
                [320, 100]
            ]
        },
        "ei_article_m300x250_1": {
            'siteId': '349296',
            'size': [
                [320, 50],
                [320, 100],
                [300, 250]
            ]
        },
        "ei_article_m300x250_a_incontent": {
            'siteId': '349298',
            'size': [
                [300, 250],
                [320, 100],
                [320, 50]
            ]
        },
        "ei_article_m300x250_b_incontent": {
            'siteId': '349299',
            'size': [
                [300, 250],
                [320, 100],
                [320, 50]
            ]
        },
        "ei_article_m300x250_c_incontent": {
            'siteId': '349300',
            'size': [
                [300, 250],
                [320, 100],
                [320, 50]
            ]
        },
        "ei_article_m300x250_d_incontent": {
            'siteId': '349301',
            'size': [
                [300, 250],
                [320, 100],
                [320, 50]
            ]
        },
        "ei_article_m300x250_e_incontent": {
            'siteId': '349302',
            'size': [
                [300, 250],
                [320, 100],
                [320, 50]
            ]
        },
        "ei_article_m300x250_f_incontent": {
            'siteId': '349303',
            'size': [
                [300, 250],
                [320, 100],
                [320, 50]
            ]
        },
        "ei_recipe_d970x250_1": {
            'siteId': '349313',
            'size': [
                [970, 250],
                [970, 90],
                [728, 90]
            ]
        },
        "ei_recipe_d728x90_3": {
            'siteId': '349312',
            'size': [728, 90]
        },
        "ei_recipe_d300x600_3": {
            'siteId': '349311',
            'size': [
                [300, 250],
                [300, 600]
            ]
        },



        "ei_recipe_m300x250_1": {
            'siteId': '349314',
            'size': [
                [300, 250],
                [320, 100],
                [320, 50]
            ]
        },
        "ei_recipe_m300x250_2": {
            'siteId': '349315',
            'size': [
                [300, 250],
                [320, 100],
                [320, 50]
            ]
        },


    },
    "teads": {


        // "ei_article_d970x250_1": {
        //     "inSlot": '25437'
        // },
        //            "ei_article_d728x90_3": {
        //                "inSlot": '24553'
        //            },
        // "ei_article_d300x600_3": {
        //     "inSlot": '25244'
        // },
        // "ei_article_d300x600_4": {
        //     "inSlot": '25245'
        // },
        "ei_article_d300x250_a_incontent": {
            "placementId": '108233',
            "pageId": '99935'
        },
        "ei_article_d300x250_b_incontent": {
            "placementId": '108233',
            "pageId": '99935'
        },
        "ei_article_d300x250_c_incontent": {
            "placementId": '108233',
            "pageId": '99935'
        },
        "ei_article_d300x250_d_incontent": {
            "placementId": '108233',
            "pageId": '99935'
        },
        "ei_article_d300x250_e_incontent": {
            "placementId": '108233',
            "pageId": '99935'
        },
        "ei_article_d300x250_f_incontent": {
            "placementId": '108233',
            "pageId": '99935'
        },


        // "ei_article_m320x50": {
        //     "inSlot": '25253'
        // },
        // "ei_article_m300x250_1": {
        //     "inSlot": '25246'
        // },
        "ei_article_m300x250_a_incontent": {
            "placementId": '108233',
            "pageId": '99935'
        },
        "ei_article_m300x250_b_incontent": {
            "placementId": '108233',
            "pageId": '99935'
        },
        "ei_article_m300x250_c_incontent": {
            "placementId": '108233',
            "pageId": '99935'
        },
        "ei_article_m300x250_d_incontent": {
            "placementId": '108233',
            "pageId": '99935'
        },
        "ei_article_m300x250_e_incontent": {
            "placementId": '108233',
            "pageId": '99935'
        },
        "ei_article_m300x250_f_incontent": {
            "placementId": '108233',
            "pageId": '99935'
        },


    },
    "audienceNetwork": {
        "ei_article_m300x250_1": {
            "placementId": '850549285310657_871753746523544'
        },
        "ei_article_m300x250_2": {
            "placementId": '850549285310657_871753869856865'
        },
        "ei_article_m300x250_a_incontent": {
            "placementId": '850549285310657_871724606526458'
        },
        "ei_article_m300x250_b_incontent": {
            "placementId": '850549285310657_871724606526458'
        },
        "ei_article_m300x250_c_incontent": {
            "placementId": '850549285310657_871724606526458'
        },
        "ei_article_m300x250_d_incontent": {
            "placementId": '850549285310657_871724606526458'
        },
        "ei_article_m300x250_e_incontent": {
            "placementId": '850549285310657_871724606526458'
        },
        "ei_article_m300x250_f_incontent": {
            "placementId": '850549285310657_871724606526458'
        },
        "ei_article_m320x50": {
            "placementId": '850549285310657_871753486523570'
        },
        "ei_recipe_m300x250_1": {
            "placementId": '850549285310657_871769936521925'
        },
        "ei_recipe_m300x250_2": {
            "placementId": '850549285310657_871724606526458'
        },
    },
    "yieldmo": {
        "ei_article_m300x250_1": {
            "placementId": '2196461660058322454'
        },
        "ei_article_m300x250_a_incontent": {
            "placementId": '2196462000023438872'
        },
        "ei_article_m300x250_b_incontent": {
            "placementId": '2196462319159641626'
        },
        "ei_article_m300x250_c_incontent": {
            "placementId": '2196462652363539996'
        },
        "ei_article_m300x250_d_incontent": {
            "placementId": '2196463871261836830'
        },
        "ei_article_m300x250_e_incontent": {
            "placementId": '2196465080647452192'
        },
        "ei_article_m300x250_f_incontent": {
            "placementId": '2196465235509544482'
        },
        "ei_article_m320x50": {
            "placementId": '2196465389113345572'
        },
        "ei_recipe_m300x250_1": {
            "placementId": '2196465770744677928'
        },
        "ei_recipe_m300x250_2": {
            "placementId": '2196465946687342122'
        },
    },
    "rubicon": {
        "bidderSettings": {
            "bidCpmAdjustment": 1,
        },
        "common": {
            "accountId": '20812',
            "siteId": '258740',

        },
        "ei_article_d300x250_a_incontent": {
            "zoneId": '1284668'
        },
        "ei_article_d300x250_b_incontent": {
            "zoneId": '1284664'
        },
        "ei_article_d300x250_c_incontent": {
            "zoneId": '1284660'
        },
        "ei_article_d300x250_d_incontent": {
            "zoneId": '1284656'
        },
        "ei_article_d300x250_e_incontent": {
            "zoneId": '1284654'
        },
        "ei_article_d300x250_f_incontent": {
            "zoneId": '1284652'
        },
        "ei_article_d300x600_3": {
            "zoneId": '1284650'
        },
        "ei_article_d300x600_4": {
            "zoneId": '1284648'
        },
        "ei_article_d970x250_1": {
            "zoneId": '1284646'
        },
        "ei_article_d970x250_3": {
            "zoneId": '1284644'
        },
        "ei_article_m300x250_1": {
            "zoneId": '1284642'
        },
        "ei_article_m300x250_2": {
            "zoneId": '1284636'
        },
        "ei_article_m300x250_a_incontent": {
            "zoneId": '1284666'
        },
        "ei_article_m300x250_b_incontent": {
            "zoneId": '1284632'
        },
        "ei_article_m300x250_c_incontent": {
            "zoneId": '1284630'
        },
        "ei_article_m300x250_d_incontent": {
            "zoneId": '1284624'
        },
        "ei_article_m300x250_e_incontent": {
            "zoneId": '1284622'
        },
        "ei_article_m300x250_f_incontent": {
            "zoneId": '1284620'
        },
        "ei_article_m320x50": {
            "zoneId": '1284618'
        },

        "ei_recipe_d300x600_3": {
            "zoneId": '1284612'
        },
        "ei_recipe_d728x90_3": {
            "zoneId": '1284610'
        },
        "ei_recipe_d970x250_1": {
            "zoneId": '1284608'
        },


        "ei_recipe_m300x250_1": {
            "zoneId": '1284606'
        },
        "ei_recipe_m300x250_2": {
            "zoneId": '1284628'
        },


    },
    "gourmetads": {
        "bidderSettings": {
            "bidCpmAdjustment": 0.8,
        },
        "common": {
            "usePaymentRule": true,
        },

        "ei_article_d300x250_a_incontent": {
            "placementId": '15615751'
        },
        "ei_article_d300x250_b_incontent": {
            "placementId": '15615742'
        },
        "ei_article_d300x250_c_incontent": {
            "placementId": '15615732'
        },
        "ei_article_d300x250_d_incontent": {
            "placementId": '15615724'
        },
        "ei_article_d300x250_e_incontent": {
            "placementId": '15615715'
        },
        "ei_article_d300x250_f_incontent": {
            "placementId": '15615709'
        },
        "ei_article_d300x600_3": {
            "placementId": '15615691'
        },
        "ei_article_d300x600_4": {
            "placementId": '15615685'
        },
        "ei_article_d970x250_1": {
            "placementId": '15615518'
        },
        "ei_article_d970x250_3": {
            "placementId": '15615505'
        },


        "ei_article_m300x250_1": {
            "placementId": '15615495'
        },
        "ei_article_m300x250_a_incontent": {
            "placementId": '15615482'
        },
        "ei_article_m300x250_b_incontent": {
            "placementId": '15615469'
        },
        "ei_article_m300x250_c_incontent": {
            "placementId": '15615442'
        },
        "ei_article_m300x250_d_incontent": {
            "placementId": '15615435'
        },
        "ei_article_m300x250_e_incontent": {
            "placementId": '15615380'
        },
        "ei_article_m300x250_f_incontent": {
            "placementId": '15615375'
        },
        "ei_article_m320x50": {
            "placementId": '15615360'

        },



        "ei_recipe_d300x600_3": {
            "placementId": '15615336'
        },
        "ei_recipe_d728x90_3": {
            "placementId": '15615331'
        },
        "ei_recipe_d970x250_1": {
            "placementId": '15615327'
        },
        "ei_recipe_m300x250_1": {
            "placementId": '15615320'
        },
        "ei_recipe_m300x250_2": {
            "placementId": '15615306'
        },

    }
};