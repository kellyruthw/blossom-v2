import trackingBuilder from "../../tracking-central";

export default function (pbjs) {


    pbjs.onEvent('auctionEnd', function (data) {
        data.adUnitCodes.forEach((placementId) => {

            document.getElementById(placementId).dataset.auctionId = data.auctionId;
        });
        let auctionReport = [];
        let eventName = "prebidAuction";
        data.bidderRequests.forEach((bidReq) => {
            let {
                bidderCode
            } = bidReq
            auctionReport[bidderCode] = {
                bidderCode
            };
        });
        let adId;
        data.bidsReceived.forEach((bidRec) => {
            let {
                bidderCode,
                timeToRespond,
                cpm,
                adUnitCode
            } = bidRec;
            adId = document.getElementById(adUnitCode).dataset.adId;
            auctionReport[bidderCode] = {
                bidderCode,
                timeToRespond,
                cpm
            };
        });
        let payload = Object.values(auctionReport);
        console.log("this is payload : ", payload);
        trackingBuilder(eventName).auctionId(data.auctionId).adId(adId).body(payload).send();


    });

}