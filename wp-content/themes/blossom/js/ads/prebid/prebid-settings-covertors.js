/* eslint-disable array-bracket-newline */
import {
    prebids
} from "./prebid-conf";
import {
    getAdUnitNameFromAdUnitCode
} from "..";
import {
    dfpSettings
} from '../ads-settings'


let convertedPrebidSettings = convertToAdUnitsForBidder(extrapulateCommon(prebids));

function extrapulateCommon(prebids) {

    for (let network of Object.values(prebids)) {
        if (Reflect.apply(Object.hasOwnProperty, network, ["common"])) {
            for (let [key, value] of Object.entries(network)) {
                if (key !== "common" && key !== "bidderSettings") {
                    value = Object.assign(value, network.common)
                }
            }
            Reflect.deleteProperty(network, "common");
        }
    }
    return prebids;
}



function convertToAdUnitsForBidder(prebids) {
    let convertedPrebids = {};
    for (let [network, setup] of Object.entries(prebids)) {
        for (let [adUnit, params] of Object.entries(setup)) {
            convertedPrebids[adUnit] = convertedPrebids[adUnit] || {};
            convertedPrebids[adUnit][network] = params;
        }
    }
    return convertedPrebids;
}

function prepareBidsForAuction(prebidSettings, adUnitName) {
    let bidsArr = [];
    let biddersConf = prebidSettings[adUnitName];
    for (let [bidder, params] of Object.entries(biddersConf)) {
        bidsArr.push({
            bidder,
            params
        })
    }
    return bidsArr;

}



function buildAdUnitsFromPlacements(prebidSettings, dfpSettings, adUnitCode) {
    let adUnitName = getAdUnitNameFromAdUnitCode(adUnitCode);
    let sizes = dfpSettings[adUnitName].size;
    sizes = sizes.filter((size) => {
        if (size === 'fluid') {
            return false;
        }
        return true;
    });

    return [{
        code: adUnitCode,
        mediaTypes: {
            banner: {
                sizes
            }
        },
        bids: prepareBidsForAuction(prebidSettings, adUnitName)
    }];
}
export const getHeaderBiddingForAdUnitCode = (adunitCode) => {
    return buildAdUnitsFromPlacements(convertedPrebidSettings, dfpSettings, adunitCode);

}

export function getPrebidBiddersCustomSettings() {
    let ret = {};
    let {
        bidderSettings
    } = convertedPrebidSettings;
    Object.keys(bidderSettings).forEach((bidderSettingKey) => {
        if ("bidCpmAdjustment" in bidderSettings[bidderSettingKey]) {
            let val = parseFloat(bidderSettings[bidderSettingKey].bidCpmAdjustment);
            ret[bidderSettingKey] = {};
            ret[bidderSettingKey].bidCpmAdjustment = function (bidCpm, bid) {
                console.log("bidCpmAdjustment ", val, " ", bid, bidCpm);
                return bidCpm * val;
            }
        }
    });
    return ret;
}