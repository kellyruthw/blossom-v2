const cpmModifier = 0.01;
export const advertisersMap = {
    "11307702": "babyvfirstt",
    "4570323779": "adx fm",
    "4602399567": "spotx",
    "4604226884": "prebid",
    "4641427377": "first media",
    "4725623496": "amazon",
    "4745761096": "target"
};

export function generateWinningAdvertiser(prebidderInfo, event) {
    let cpm, bidderCode;
    if (!prebidderInfo) {
        cpm = cpmModifier;
        bidderCode = advertisersMap[event.advertiserId];
        return {
            cpm,
            bidderCode
        };
    }
    if (advertisersMap[event.advertiserId] === "prebid") {
        return prebidderInfo;
    }
    bidderCode = advertisersMap[event.advertiserId];
    cpm = prebidderInfo.cpm + cpmModifier;
    return {
        cpm,
        bidderCode
    };

}