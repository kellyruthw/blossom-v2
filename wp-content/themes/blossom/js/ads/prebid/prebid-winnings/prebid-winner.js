import {
    generateWinningAdvertiser
} from "./advertisers-info";

export const getWinningPreBidder = function (event) {
        let prebidder = null;
        let winningBidders = window.pbjs.getHighestCpmBids();
        winningBidders.forEach((winningBidder) => {
                if (winningBidder.adUnitCode === event.slot.getSlotElementId()) {

                    prebidder = winningBidder;
                }
            });

            prebidder = generateWinningAdvertiser(prebidder, event) 
            return prebidder;


        };