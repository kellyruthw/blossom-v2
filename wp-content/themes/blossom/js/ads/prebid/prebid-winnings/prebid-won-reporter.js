import trackingBuilder from "../../../tracking-central/index"
import { fbqWatcher, obApiWatcher } from "../../../3rd-parties-objects-watchers";

export default function (pbjs) {

    pbjs.onEvent('bidWon', function (bid) {
        let adWrapper = document.getElementById(bid.adUnitCode);
        let auctionId = adWrapper.dataset.auctionId;
        let adId = adWrapper.dataset.adId;
        let adUnit = adWrapper.dataset.adUnit;
        trackingBuilder("prebidWon").bidder(bid.bidderCode)
            .adunit(adUnit)
            .adUnitCounter(adWrapper.dataset.slotDefineCounter)
            .adId(adId)
            .auctionId(auctionId)
            .revenue(bid.cpm).extras(bid.auctionId, bid.adId).send();
        pixelCpmForFacebookPrebidwon(bid.cpm);
    });

    let buckets = [];
    let accumaltedCpm = 0;
    const cpmPerPixelFactor = 5

    function calcHowManyBucketsNeeded() {

        return parseInt(accumaltedCpm / cpmPerPixelFactor) + 1;
    }

    function pixelCpmForFacebookPrebidwon(cpm) {
        accumaltedCpm += cpm;
        let numOfBucketShouldBe = calcHowManyBucketsNeeded();
        let actualNumberOfBuckets = buckets.length;
        for (let i = actualNumberOfBuckets; i < numOfBucketShouldBe; i++) {
            buckets.push(true);
            let val = buckets.length * cpmPerPixelFactor;
            fbqWatcher.push(()=>{
                window.fbq('trackCustom', 'revenePerUserFb', {
                    "cpmBucket": val
                });

            });
            
            obApiWatcher.push(()=>{
                window.obApi('track', 'revenue ' + val);

            });
            
        }
    }



}