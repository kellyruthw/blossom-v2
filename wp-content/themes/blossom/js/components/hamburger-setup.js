import $ from 'jquery';
export default () => {
    var $hamburger = $(".hamburger");
    var $menu = $("header nav");
    $hamburger.on("click", function () {
        $hamburger.toggleClass("is-active");
        $menu.show("fast");
    });
}