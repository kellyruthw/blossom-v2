import setupHamburger from './hamburger-setup';
import setupSearch from './search-setup';
import setupModal from './modal'


export default (window)=>{

    setupHamburger();
    setupSearch(window);
    setupModal(window);
};
