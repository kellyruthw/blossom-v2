import { loadInstagramEmbed } from "./lazy-instagram";
import { loadTwiterEmbed } from "./lazy-twitter";

  
  function loadEmbeds(){
    
    loadInstagramEmbed();
    loadTwiterEmbed();
    //instagra, fix//
    setInterval(function () {
      if (typeof instgrm !== "undefined") {
          window.instgrm && window.instgrm.Embeds && window.instgrm.Embeds.process();
      }
      if (typeof window.Playbuzz !== "undefined" && window.Playbuzz.render) {
          window.Playbuzz.render()
      }
  }, 5000);
  
  }

  export {loadEmbeds};