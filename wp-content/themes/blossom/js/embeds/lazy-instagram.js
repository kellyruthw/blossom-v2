import jQuery from 'jquery';
import { ViewPortManger, getScrollHandler } from '../utils';
import { SELECTOR_ROOT_LOADER } from '../load-next-article';
function loadInstagram(el) {


  if (el.dataset.soyummyLazy) {
    return;
  }
  
  jQuery.get("https://api.instagram.com/oembed?url=http://instagr.am/p/" + el.id, function (data) {
    jQuery("#" + el.id).append(data.html);
  });
  el.dataset.soyummyLazy = true;


}



export const loadInstagramEmbed = ()=> {





  let instaViewPortManager = new ViewPortManger('.bu-social-embed-instagram', SELECTOR_ROOT_LOADER, true , window.innerHeight * 2.5);
  instaViewPortManager.setCallbackInViewPort((instaWrapper) => {

    loadInstagram(instaWrapper);
  });



  getScrollHandler()
    .addCallback(function () {
      instaViewPortManager.run();
    });




}