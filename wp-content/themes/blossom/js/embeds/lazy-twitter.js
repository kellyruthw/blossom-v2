import jQuery from 'jquery';
import {
  ViewPortManger,
  getScrollHandler
} from '../utils';
import {
  SELECTOR_ROOT_LOADER
} from '../load-next-article';




function loadTwitter(el) {


  if (el.dataset.soyummyLazy) {
    return;
  }
  var sizeWidth = (screen.width > 780) ? 500 : 350;

  jQuery.get("/twitter-serve.php/?id=" + el.id + "&width=" + sizeWidth, function (data) {
    jQuery("#" + el.id).append(data.html);
  });
  el.dataset.soyummyLazy = true;


}






export const loadTwiterEmbed = () => {





  let twitterViewPortManager = new ViewPortManger('.bu-social-embed-twitter', SELECTOR_ROOT_LOADER, true, window.innerHeight * 5);
  twitterViewPortManager.setCallbackInViewPort((twitterWrapper) => {

    loadTwitter(twitterWrapper);
  });



  getScrollHandler()
    .addCallback(function () {
      twitterViewPortManager.run();
    });




}