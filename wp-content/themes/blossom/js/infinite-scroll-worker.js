import InfiniteScroll from "infinite-scroll";
let articlesElem = document.querySelector('.articles');
export default () => {
    if (articlesElem) {
        new InfiniteScroll(articlesElem, {
            // options
            path: '.next a',
            append: '.articles article',
            prefill: true,
            history: false,
            hideNav: '.navigation',
            button: '.view-more .btn',
            // load pages on button click
            scrollThreshold: false,
            status: '.page-load-status',
            // load pages on button click

        });

    }

};