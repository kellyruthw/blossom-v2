

import {
    ViewPortManger,
    getScrollHandler,
    SELECTOR_ROOT_LOADER
  } from './lazy.classes';
  
  
  
  
  function loadTaboola(el) {
  
  
    let isLoadedAlready = el.getAttribute('soyummy-lazy');
    if (isLoadedAlready) {
      return;
    }
  

    //taboola 
	window._taboola = window._taboola || [];
	let tab_placement = window.location.href;

	_taboola.push({
		mode: 'thumbnails-a',
		container: 'taboola-below-article-thumbnails-2nd',
		placement: tab_placement + '_2nd',
		target_type: 'mix'
	});
    
    el.setAttribute('soyummy-lazy', true);

	
  
  
  }
  
  
  
  
  
  
  export default function (jQuery , preRunCacllback = null) {
    
    preRunCacllback && preRunCacllback();
  
  
    
    let taboolaViewPortManager = new ViewPortManger('#taboola-below-article-thumbnails-2nd', SELECTOR_ROOT_LOADER, true  );
    taboolaViewPortManager.setCallbackInViewPort((taboolaWrapper) => {
  
      loadTaboola(taboolaWrapper);
    });
  
  
  
    getScrollHandler()
      .addCallback(function () {
        taboolaViewPortManager.run();
      });
      
  
  
  
  }