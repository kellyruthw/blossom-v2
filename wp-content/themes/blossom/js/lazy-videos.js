
  
  import {
    loadVideoIfNeeded,
    videoPlayrs
  } from './Video-central';
import { ViewPortManger, getScrollHandler } from './utils';
import { SELECTOR_ROOT_LOADER } from './load-next-article';
  
  
  
  export default function (jQuery, preRunCacllback = null) {
    loadVideoIfNeeded(".single-video");
    preRunCacllback && preRunCacllback();
  
  
    
  
    let videosViewportManager = new ViewPortManger(".video-wrapper",
      SELECTOR_ROOT_LOADER, true, 0, () => {
        loadVideoIfNeeded(true, ".single-video");
      });
    videosViewportManager.disableOptimizePeformance()
      .setCallbackInViewPort((element) => {
        let playerInstance = videoPlayrs[element.id];
        if (playerInstance && playerInstance.getState() !== "playing") {
          if (!playerInstance.pauseReason || (playerInstance.pauseReason !== "interaction" )) {
            let viewable = playerInstance.getViewable();
            if(viewable){
              playerInstance.play();
              
              if(!playerInstance.viewport){
                //trackingBuilder("video_in_viewport").send();
                playerInstance.viewport = true;
              }
              
              
              
  
            }
            
          }
  
        }
        
      })
      .setCallbackNotInViewPort((element) => {
        let playerInstance = videoPlayrs[element.id];
        if (playerInstance && playerInstance.getState() !== "paused" && playerInstance.getState() !== "idle") {
          playerInstance.pause();
          
          
          
        }
        
      })
  
  
  
  
  
  
    getScrollHandler()
      .addCallback(function () {
        videosViewportManager.run();
      });
  
  
  
  }