import {
    //isMobile
} from "is-mobile";
import {
    ViewPortManger,
    getScrollHandler
} from "./utils";
import {
    isArticlePage
} from "./page-types";
import $ from 'jquery';

export const SELECTOR_ROOT_LOADER = "#main";

let nextUrl = $(".article-url")[0] ? $(".article-url")[0].dataset.nextArticleSlug : "";
function ajaxNextArticle() {


    $.ajax({
        url: nextUrl,
        type: 'GET',
        dataType: "html",
        success: function (output) {
            if (output) {
                let toAppend = $(output).find("#main").eq(0).children();
                nextUrl = $(output).find(".article-url")[0].dataset.nextArticleSlug;
                console.log("ronennnnn " , $(toAppend));
                $(SELECTOR_ROOT_LOADER).find('.more-to-love').last().after(toAppend); // where to insert posts

            }
        }
    });
}

export default () => {

    if (
        //isMobile() && 
        isArticlePage) {
        let isFirstTimeRanAlready = false;
        let checkViewportForAjaxLoading = (new ViewPortManger('.ajax-loader-anchor', SELECTOR_ROOT_LOADER, true, window.innerHeight * 1));
        checkViewportForAjaxLoading.setCallbackInViewPort((item) => {

            if (item.dataset.ajaxLoaded) {

                return;
            }
            if (!isFirstTimeRanAlready) {
                setTimeout(() => {
                    ajaxNextArticle();
                    isFirstTimeRanAlready = true;
                }, 5000);
            } else {
                ajaxNextArticle();
            }

            item.dataset.ajaxLoaded = true;
        });

        getScrollHandler().addCallback(function () {
            checkViewportForAjaxLoading.run();

        });

    }
}