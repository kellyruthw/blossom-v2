console.log("ronen");
import '@babel/polyfill';
import 'promise-polyfill/src/polyfill';

require('./utils/console-manager');
require("./ads/prebid/prebid.lib");
require("./navigation");
import {
    loadEmbeds
} from "./embeds";
import initComponents from "./components";
import runInfiniteScrollWorker from "./infinite-scroll-worker";
import runSocialWorker from "./social-share-worker";

import {
    AdManager
} from "./ads";
import {
    reportPageView
} from './tracking-central';
import {
    loadVideoIfNeeded
} from "./Video-central";
import runLazyVideos from './lazy-videos';
import prebidWonReporter from './ads/prebid/prebid-winnings/prebid-won-reporter';
import prebidResponseCapture from './ads/prebid/prebid-reponse-capture';
import subheaderReporter from './subheader-reporter';
import runLoadMore from './load-next-article';
import { navigationInfiniteUnitRunner } from './url-manager';
import initAdditionalGa  from './tracking-central/google-analytics-reporter';
import { ppWatcher } from './3rd-parties-objects-watchers';
console.log("this is test");

initAdditionalGa(window);
//loadEmbeds();
initComponents(window);
runInfiniteScrollWorker();
runSocialWorker();
new AdManager(window);
prebidWonReporter(window.pbjs);
prebidResponseCapture(window.pbjs);
reportPageView();
loadVideoIfNeeded(true);
runLazyVideos();
subheaderReporter();
runLoadMore();
navigationInfiniteUnitRunner();


ppWatcher.push(()=>{
    window.PPTracker.setFacebookRevenueReportingStatus(true);
});

