import { wpEnv } from "./wordpress-env";

export const isHomePage = wpEnv.isHomePage;
export const isMobile = wpEnv.isMobile === "true" ? true:false;
export const pageType = wpEnv.pageType;
export const postId = wpEnv.postId;
export const isSponsored = wpEnv.isSponsored;
export const currentPageType = pageType;



let pageTypesConsts = {};
pageTypesConsts.ARTICLE = 'article';
pageTypesConsts.RECIPE = 'recipe';
pageTypesConsts.CATEGORY = 'category';
pageTypesConsts.HOMEPAGE = 'page';
export {pageTypesConsts};



export const isArticlePage = pageType === pageTypesConsts.ARTICLE;
export const isRecipePage = pageType === pageTypesConsts.RECIPE;
export const isCategoryPage = pageType === pageTypesConsts.CATEGORY;


