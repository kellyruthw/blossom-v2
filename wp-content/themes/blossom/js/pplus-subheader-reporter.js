/* eslint-disable no-console */
/* lazyload.js 
 *
 *
 * expects a list of:  
 * `<img src="blank.gif" data-src="my_image.png" width="600" height="400" class="lazy">`
 */


import {
    ViewPortManger,
    getScrollHandler,
    
  } from './utils/index';
import { SELECTOR_ROOT_LOADER } from './load-next-article';
import { ppWatcher } from './3rd-parties-objects-watchers';

  
  let isFirstTimeReportedAlready = false;
  
  
  
  
  function reportPubplus(el) {
  
  
    if(el.textContent && el.textContent.includes("to our newsletter")){
      //console.log("skipping newsletter repport page view for pubplus :" , el);
      return;
  
    }
    if(!isFirstTimeReportedAlready){
      console.log("skipping first time repport page view for pubplus :" , el);
      isFirstTimeReportedAlready = true;
      return;
    }
    let isLoadedAlready = el.dataset.soyummyLazyPplus;
    if (isLoadedAlready) {
      return;
    }
    console.log("about to report puplus page view for el : " , el);
    ppWatcher.push(()=>{
      window.PPTracker.push('page_view');

    });
    
    el.dataset.soyummyLazyPplus = true;
  
  
  }
  
  
  
  
  export default function (   ) {
    
  
    let pplusPageViewReportViewPortManager = new ViewPortManger('h1  , .entry-content h2 , .entry-content  h3 ', SELECTOR_ROOT_LOADER, true);
    pplusPageViewReportViewPortManager.setCallbackInViewPort((titleEl) => {
      //console.log("debug_Imgs_in_view_port: ", imagesViewPortManager.itemsStatusInViewPortAlready);
  
      reportPubplus(titleEl );
    });
  
  
    getScrollHandler()
      
      .addCallback(function () {
        pplusPageViewReportViewPortManager.run();
      });
      
  
  
  
  }