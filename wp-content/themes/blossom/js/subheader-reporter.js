



  import trackingBuilder from './tracking-central';

  
  
import { ViewPortManger, getScrollHandler } from './utils';
import { SELECTOR_ROOT_LOADER } from './load-next-article';
  
  
  
  function reportSubheader(el) {
  
  
    if(el.textContent && el.textContent.includes("to our newsletter")){
      return;
  
    }
    
    let isLoadedAlready = el.dataset.soyummyLazySubheader;
    if (isLoadedAlready) {
      return;
    }
    
    el.dataset.soyummyLazySubheader = true;
    trackingBuilder("subheader").send();
  
  
  }
  
  
  
  
  export default function ( ) {
    let subheaderReportViewPortManager = new ViewPortManger('.entry-content h2 , .entry-content  h3 ', SELECTOR_ROOT_LOADER, true);
    subheaderReportViewPortManager.setCallbackInViewPort((titleEl) => {
  
      reportSubheader(titleEl );
    });
  
  
  
    getScrollHandler()
      .addCallback(function () {
        subheaderReportViewPortManager.run();
      });
      
  
  
  
  }