
import { trackingVersion } from "./version";
import { additionalGaCostumEvent } from "./google-analytics-reporter";


let pageViewport = 0;
let lastPrevArticlesViewportCounts = 0;
let articleName = location.pathname;
export let origArticle = articleName.replace(/\//g,"");
let articlesStack = [{"viewport" : 0}];
export let articleBefore = "";



export function setDepth(){
    
    lastPrevArticlesViewportCounts = pageViewport;
    articlesStack.push({ "viewport" : 0});

}

export function setArticleBefore(val){
    articleBefore = val;
}



export function setViewport(_pageViewport){
    pageViewport = _pageViewport;
    articlesStack[articlesStack.length - 1]["viewport"] = pageViewport - lastPrevArticlesViewportCounts;
}

export function representPageDepth(){
    return "page_" + articlesStack.length;
}

export const isCurrOrigArticle = function(){
    let currentArticle = location.pathname;
    let origArticle = articleName.replace(/\//g,"");
    currentArticle = currentArticle.replace(/\//g,"");
    return origArticle === currentArticle;

};

export  function getCurrArticleState(){
    let currentArticle = location.pathname;
    let origArticle = articleName.replace(/\//g,"");
    currentArticle = currentArticle.replace(/\//g,"");
    let ret = articlesStack[articlesStack.length - 1];
    ret.currentArticle = currentArticle;
    ret.name = currentArticle;
    ret.originArticle = origArticle;
    ret.representivePageDepth = representPageDepth();
    ret.representiveViewportDepth = "viewport_" + (ret.viewport + 1);
    return  ret;
}

export function tempReportGooglePatch(eventName){
    let currArticleData = getCurrArticleState();
    let {
        name,
        representivePageDepth,
        representiveViewportDepth
    } = currArticleData;
    let currentArticle = location.pathname;
    let origArticleName = name;
    origArticleName = origArticleName.replace(/\//g,"");
    currentArticle = currentArticle.replace(/\//g,"");

    
    additionalGaCostumEvent( eventName+"_" + trackingVersion ,currentArticle , origArticleName  );
    


    
    
}



