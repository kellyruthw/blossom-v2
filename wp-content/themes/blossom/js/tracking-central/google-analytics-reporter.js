import {
    getUserLast2digitInt
} from "../user-manager";
import jQuery from 'jquery';
import {
    gaWatcher
} from "../3rd-parties-objects-watchers";

const site = "soyummy2.com";

const gaClientName = "privateGa";
let goCondition = document.URL.includes(site) && getUserLast2digitInt() > 89;
let throttle = function (func, throttleInMilis) {
    let lastVal = null;
    let isThrottled = false;

    return function (...args) {
        if (isThrottled) {
            return lastVal;
        }
        isThrottled = true;
        lastVal = Reflect.apply(func, this, args);
        setTimeout(() => {
            isThrottled = false;
        }, throttleInMilis);
        return lastVal;
    };
};
export const additionalGaCostumEvent = function (eventName, eventCategory, eventAction, eventLabel) {
    if (!goCondition) {
        return;
    }
    gaWatcher.push(() => {
        window.ga(gaClientName + '.send', 'event', eventName, eventCategory, eventAction, eventLabel);

    });


}

export const additionalGaPageView = function () {
    if (!goCondition) {
        return;
    }
    gaWatcher.push(() => {
        window.ga(gaClientName + '.send', 'pageview');
    });
}

export default (window) => {
    
    if (!goCondition) {
        return;
    }
    gaWatcher.push(() => {
        window.ga('create', 'UA-141767756-1', 'auto', gaClientName);
        window[gaClientName] = window.ga;
    });

    

    additionalGaPageView();
    let reportedScrolls = [];
    var viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

    let reportScrollPercentage = function () {

        let footerContainer = document.querySelector('footer');
        let percent = Math.floor((scrollY + viewportHeight) / (document.body.clientHeight - footerContainer.clientHeight -
            50) * 100 / 20) * 20;
        percent = Math.min(100, Math.max(0, percent));
        if (reportedScrolls.indexOf(percent) === -1) { //report
            reportedScrolls.push(percent);
            //console.log('Scroll Event (' + percent + '%) Sent');
            additionalGaCostumEvent('Scroll', '' + percent + '%');
        }
    }
    window.addEventListener('scroll', throttle(reportScrollPercentage, 500));
    reportScrollPercentage();

    document.addEventListener("visibilitychange", function () {
        additionalGaCostumEvent('visibilityState', document.visibilityState, document.URL);

    });

    jQuery("a").click(function (e) {
        additionalGaCostumEvent('clk', e.target.href, document.URL);
    });


};