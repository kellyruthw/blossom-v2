import formurlencoded from 'form-urlencoded';

import {
    isArticlePage,
    pageType
} from '../page-types';


import {
    getUrlParams,
    mysqlEscape,
    
} from '../utils';
import {
    Md5
} from 'ts-md5/dist/md5';
import moment from 'moment';
import { getCurrArticleState } from './article-state';
import { trackingVersion } from './version';
import { getUserid } from '../user-manager';










const TRACKING_URL = "https://tracking.fmtracking.net/inserttracking/?";

const initTime = new Date().getTime();
const sessionId = new Date().getTime() + "" + Math.random() * 1e17;
let pageViewId = new Date().getTime() + "" + Math.random() * 1e17;
const urlParams = getUrlParams();





const timeInSession = () => {
    let now = new Date().getTime();
    let ret = now - initTime;
    //return parseInt(ret/1000);
    return ret;

}

const genSiteId = () => {
    if (location.host.includes("soyummy")) {
        return 1;
    }
    if (location.host.includes("blossom")) {
        return 2;
    }

    return 0;

};




class TrackingBuilder {
    constructor() {
        this.params = {};

        this.init();
    }

    init() {
        this.extraParams = [];
        this.params.userAgent = navigator.userAgent;
        this.params.lang = navigator.language;
        this.params.screenSize = screen.width + "x" + screen.height;
        this.params.referrer = document.referrer;
        this.params.url = document.URL;
        this.params.version = trackingVersion;
        this.params.uid = getUserid;
        this.params.pageViewId = pageViewId;
        this.params.sessionId = sessionId;
        this.params.pageType = pageType === "page" ? "homepage" : pageType;
        this.params.abExp = global.abExp;
        this.params.abGroup = global.abGroup;
        this.params.utmSource = urlParams['utm_source'];
        this.params.utmMedium = urlParams['utm_medium'];
        this.params.utmCampaign = urlParams['utm_campaign'];
        this.params.dmAdId = urlParams['utm_content'];
        this.params.dmCampaignId = urlParams['utm_medium'];
        this.params.dmAdsetId = urlParams['utm_campaign'];
        this.params.clientDateTime = moment().format('YYYY-MM-DDTHH:mm:ssZ');
        this.params.siteId = genSiteId();
        if (urlParams["utm_campaign"]) {
            this.params.utmCampaignHash = Md5.hashStr("'" + decodeURIComponent(urlParams["utm_campaign"]) + "'");
        }

    }

    eventName(val) {
        this.params.eventName = val;
        return this;
    }

    revenue(val) {
        this.params.revenue = val;
        return this;
    }

    bidder(val) {
        this.params.bidder = val;
        return this;
    }
    adunit(val) {
        this.params.adunit = val;
        return this;
    }

    adId(val) {
        this.params.adId = val;
        return this;

    };

    auctionId(val) {
        this.params.auctionId = val;
        return this;

    };

    adUnitCounter(val){
        this.params.adUnitCounter = val;
        return this;

    }

    body(val) {
        this.bodyReq = val;
        return this;
    }

    generatePageViewId(){
        pageViewId = new Date().getTime() + "" + Math.random() * 1e17;
        this.params.pageViewId = pageViewId;
        return this;
    }


    extras(...args) {
        args = args.filter(elem => {
            return typeof elem !== "undefined" && elem !== null
        })
        this.extraParams = [...this.extraParams, ...args];
        return this;

    }

    stripExtras() {
        this.extraParams.forEach((arg, index) => {
            index = index + 1;
            this.params["s" + index] = mysqlEscape(arg);
        });
        return this;

    }

    handleArticlePage() {
        let {
            originArticle,
            currentArticle,
            representivePageDepth,
            representiveViewportDepth
        } = getCurrArticleState();
        this.extras(representivePageDepth, representiveViewportDepth);
        this.params.originArticle = mysqlEscape(originArticle);
        this.params.currentArticle = mysqlEscape(currentArticle);
        this.params.articleId = Md5.hashStr(originArticle);
    }

    preSend() {
        if (isArticlePage) {
            this.handleArticlePage()
        }

    }

    send() {


        this.preSend();
        this.params.timeInSession = timeInSession();
        this.stripExtras();


        if (this.bodyReq) {
            this.sendAjax();
            return;
        }
        let img = document.createElement("img");
        img.setAttribute("style", "width:1px;solid:1px; visibility:hidden;position:relative;top:-10000px;left:-10000px");
        document.body.appendChild(img);
        img.onload = () => {
            document.body.removeChild(img);
            img = null;
        };
        img.onerror = img.onload;
        img.src = TRACKING_URL + formurlencoded(this.params);

    }

    sendAjax() {

        let xhr = new XMLHttpRequest();
        let url = TRACKING_URL + formurlencoded(this.params);
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                //let json = JSON.parse(xhr.responseText);
            }
        }
        let data = JSON.stringify(this.bodyReq);
        xhr.send(data);
        
    }
};

export default function trackingBuilder(eventName) {

    return new TrackingBuilder().eventName(eventName);

};

export const reportPageView = (generatePageViewId = false)=>{
    let trckBuilder = trackingBuilder("pageView");
    if(generatePageViewId){
        trckBuilder.generatePageViewId();
    }
    trckBuilder.send();
};

