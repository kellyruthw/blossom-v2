import {
    ViewPortManger,
    getScrollHandler,
    
} from './utils';



import trackingBuilder, {
    reportPageView
} from './tracking-central';


import { additionalGaCostumEvent, additionalGaPageView } from './tracking-central/google-analytics-reporter';
import { isArticlePage } from './page-types';
import { SELECTOR_ROOT_LOADER } from './load-next-article';
import { trackingVersion } from './tracking-central/version';
import { getCurrArticleState, tempReportGooglePatch, setViewport, representPageDepth, setArticleBefore, setDepth , articleBefore } from './tracking-central/article-state';
import { fbqWatcher } from './3rd-parties-objects-watchers';

function wrapWithQueryUrl(item) {
    return `${item}${location.search}`;

}


let didSendTracking = {};
const timestart = new Date().getTime();



//setTimeout(function () {
export const navigationInfiniteUnitRunner =  ()=>{


    if (isArticlePage) {
        getScrollHandler()
            .addCallback(function () {
    
                let indexViewport = window.parseInt(scrollY / document.documentElement.clientHeight);
                if (indexViewport && !didSendTracking[String(indexViewport)]) {
                    let secondsInArticle = ((new Date().getTime()) - timestart) / 1000;
                    let data = {
                        screenWidth: window.screen.width,
                        viewportWidth: document.documentElement.clientWidth,
                        viewportCount: indexViewport,
                        secondsInArticle
                    };
                    fbqWatcher.push(()=>{
                        window.fbq('trackCustom', 'usrScrllDpthFb_v2', data);
                    });
                    
    
                    let currArticleData = getCurrArticleState();
                    let {
                        name,
                        representivePageDepth,
                        representiveViewportDepth
                    } = currArticleData;
                    tempReportGooglePatch("endlessDepth");
                    trackingBuilder("endlessDepth").send();
                    additionalGaCostumEvent( "endless_viewport_" + name, representivePageDepth, representiveViewportDepth);
                    didSendTracking[String(indexViewport)] = true;
                    setViewport(indexViewport);
    
                }
            });
    
    
        let didSendInfiniteTracking = {};
    
        //location.hash = "";
        let checkViewport = (new ViewPortManger('.article-url', SELECTOR_ROOT_LOADER, true, 0)).disableOptimizePeformance();
        additionalGaCostumEvent( "endless_pageview_" + trackingVersion + "_" + location.pathname, representPageDepth(Object.keys(didSendInfiniteTracking).length), document.URL);
        fbqWatcher.push(()=>{
            window.fbq('trackCustom', "endless_pageview_fb_" + representPageDepth(Object.keys(didSendInfiniteTracking).length));

        });
    
        didSendInfiniteTracking[location.pathname] = {
            articleBefore
        };
    
    
    
        checkViewport.setCallbackInViewPort((item) => {
            let top = window.innerHeight / 2
            let dividingLineTop = item.getBoundingClientRect().top;
            let screenMediatorPassedDividingLine = top - dividingLineTop > 0;
            let candidate = "/" + item.dataset.articleUrl;
    
    
            if (!screenMediatorPassedDividingLine &&
                Object.keys(didSendInfiniteTracking).length > 1 &&
                location.pathname.includes(item.dataset.articleUrl)) {
                console.log("history in back : ", item.dataset.articleUrl);
                let articleGoBackTo = didSendInfiniteTracking[location.pathname].articleBefore;
                articleGoBackTo && history.replaceState(null, "", wrapWithQueryUrl(articleGoBackTo));
    
                return;
    
            } else if (screenMediatorPassedDividingLine && !(location.pathname.includes(item.dataset.articleUrl))) {
                console.log("history in next : ", item.dataset.articleUrl);
                setArticleBefore(location.pathname);
                history.replaceState(null, "", wrapWithQueryUrl(candidate));
                reportPageView(true);
                additionalGaPageView();
                
    
            } else {
    
                return;
            }
    
    
            if (screenMediatorPassedDividingLine && !(candidate in didSendInfiniteTracking)) {
                let depth = Object.keys(didSendInfiniteTracking).length;
                setDepth(depth);
                additionalGaCostumEvent( "endless_pageview_" + trackingVersion + "_" + Object.keys(didSendInfiniteTracking)[0], representPageDepth(), candidate);
                fbqWatcher.push(()=>{
                    window.fbq('trackCustom', "endless_pageview_fb_" + representPageDepth());
                });
                
    
    
                didSendInfiniteTracking[location.pathname] = {
                    articleBefore
                };
            }
        });
    
    
    
        getScrollHandler().addCallback(function () {
            checkViewport.run();
    
        });
        
    
    
    }

}



