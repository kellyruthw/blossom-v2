import { guid } from "./utils";

const UID_KEY = "uid_v2";
export const getUserid = () => {
    let uid = localStorage.getItem(UID_KEY);
    if (!uid) {
        uid = "s" + guid() + parseInt(Math.random() * 10) + "" + parseInt(Math.random() * 10);
        localStorage.setItem(UID_KEY, uid);
    }
    return uid;
};


export const getUserLast2digit = () =>{
    let uid = getUserid();
    let ret = uid.match(/\d\d$/)[0];
    console.log("**************user uid :" , ret);
    return ret;

}

export const getUserLast2digitInt = ()=>{
    return parseInt(getUserLast2digit());
};



