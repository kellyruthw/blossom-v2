if (console) {
    console.log = (function (log) {
        return function (...args) {
            if (typeof dev !== "undefined") {
                Reflect.apply(log, console, args);
            }
        }
    })(console.log);
}
//turn off/on console - dev= true;
if (document.URL.includes('dev=true') ||
    document.URL.includes("local.soyummy2.com") ||
    document.URL.includes("local.itsblossomv2.com") ||
    document.URL.includes("local.soyummy.com") ||
    document.URL.includes("staging") ||
    document.URL.includes("localhost")) {
    window.dev = true;
}