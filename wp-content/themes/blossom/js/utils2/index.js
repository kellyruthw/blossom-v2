/* eslint-disable no-control-regex */
import jQuery from 'jquery';
let scrollHandlerInstance = null;

export const getScrollHandler = () => {
    if (scrollHandlerInstance) {
        return scrollHandlerInstance;
    }
    scrollHandlerInstance = new ScrollHandler();
    return scrollHandlerInstance;

};
class ScrollHandler {
    constructor() {
        this.callbacks = [];
        this.lock = false;
        this.lastScrollTime = new Date().getTime();
        this.timeTresholdMill = 0;
        addEventListener('scroll', () => this.run());
    }

    isAllowedByTime() {
        let currentTime = new Date().getTime();
        let delta = currentTime - this.lastScrollTime;
        if (delta < this.timeTresholdMill) {
            return false;
        }
        this.lastScrollTime = currentTime;
        return true;
    }

    addCallback(_cb) {
        this.callbacks.push(_cb);
        return this;
    }

    removeCallback(_cb) {
        this.callbacks = this.callbacks.filter((cb) => {
            return cb != _cb
        });
        return this;
    }

    run() {
        if (this.lock) {
            return;
        }
        if (!this.isAllowedByTime()) {
            return;
        }
        this.lock = true;
        this.callbacks.forEach((cb) => {
            cb();
        });
        this.lock = false;
    }


}

export class ViewPortManger {

    constructor(_cssSelectorItem, _cssSelectorAjax, _shouldRunInit = false, _buffer = 0, _callbackWhenAjaxLoadMore = null) {
        this.items = [];
        this.itemsStatusInViewPortAlready = {};
        this.buffer = _buffer;
        this.cssSelectorAjax = _cssSelectorAjax;
        this.cssSelectorItem = _cssSelectorItem;
        this.lastScrollY = window.pageYOffset;
        this.shouldRunInit = _shouldRunInit;
        this.optimizedPeformance = true;
        this.callbackWhenAjaxLoadMore = _callbackWhenAjaxLoadMore;
        this.init();

    }

    init() {
        this.initItems(Array.from(jQuery(this.cssSelectorItem)));
        this.initObserver(this.cssSelectorAjax)
    }

    initItems(items) {
        items.forEach((item) => {
            item.dataset.viewportCandidate = true;
        })
        this.items = [...this.items, ...items];
    }

    initObserver(cssSelectorAjax) {
        let ajaxLoaderContainer = jQuery(cssSelectorAjax)[0];
        if (!ajaxLoaderContainer) {
            return;
        }
        let callback = (mutationsList) => {
            console.log("debug_ajax", mutationsList);
            this.callbackWhenAjaxLoadMore && this.callbackWhenAjaxLoadMore();
            let insertedElem = mutationsList[0].addedNodes[0];
            if (!insertedElem) {
                return;
            }
            if (insertedElem && insertedElem.nodeType === 3) {
                insertedElem = mutationsList[0].addedNodes[1];
            }
            let itemsToInit = jQuery(insertedElem).hasClass(this.cssSelectorItem.replace(".", '')) ? insertedElem : jQuery(insertedElem).find(this.cssSelectorItem);
            if (itemsToInit.length === 0) {
                return;
            }
            this.initItems(Array.from(jQuery(itemsToInit)));
        };
        let observer = new MutationObserver(callback);
        observer.observe(ajaxLoaderContainer, {
            childList: true,
        });

    }

    setCallbackInViewPort(_cb) {
        this.whenInViewPortCb = _cb;
        this.shouldRunInit && this.run();
        return this;
    }

    setCallbackNotInViewPort(_cb) {
        this.whenNotInViewPortCb = _cb;
        return this;
    }

    disableOptimizePeformance() {
        this.optimizedPeformance = false;
        return this;
    }

    elementInViewport(el, buffer = 0) {
        let scroll = window.scrollY || window.pageYOffset;
        let scrollDirection = scroll - this.lastScrollY;
        this.lastScrollY = scroll
        const boundsTop = el.getBoundingClientRect().top + scroll;
        const viewport = {
            top: scroll,
            bottom: scroll + window.innerHeight,
        }

        const bounds = {
            top: boundsTop,
            bottom: boundsTop + el.clientHeight,
        }

        if (scrollDirection > 0) {
            return {
                scrollDirection,
                inViewPort: (bounds.bottom >= (viewport.top - (0.5 * buffer)) && (bounds.bottom <= (viewport.bottom + buffer))) ||
                    (bounds.top <= (viewport.bottom + buffer) && (bounds.top >= (viewport.top - (0.5 * buffer))))
            };
        }
        return {
            scrollDirection,
            inViewPort: ((bounds.bottom >= (viewport.top - buffer)) && (bounds.bottom <= (viewport.bottom + (0.5 * buffer)))) ||
                ((bounds.top <= (viewport.bottom + (0.5 * buffer))) && (bounds.top >= (viewport.top - buffer)))
        };
    }

    run() {
        this.items.forEach((item, index) => {
            let {
                scrollDirection,
                inViewPort
            } = this.elementInViewport(item, this.buffer);
            if (inViewPort) {
                if (this.optimizedPeformance && item.dataset.inViewPort) {
                    return;
                }
                item.dataset.inViewPort = true;
                if (this.whenInViewPortCb) {
                    this.whenInViewPortCb(item, this.items, index, scrollDirection);
                }
            } else {
                item.dataset.inViewPort = "";
                this.whenNotInViewPortCb && this.whenNotInViewPortCb(item, this.items, index, scrollDirection);
            }
        });
    }
}

export function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + s4() + s4() + s4() + s4() + s4() + s4();
}

export function mysqlEscape(str) {
    if (typeof str !== "string") {
        return str;
    }
    // eslint-disable-next-line no-useless-escape
    return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
        switch (char) {
            case "\0":
                return "\\0";
            case "\x08":
                return "\\b";
            case "\x09":
                return "\\t";
            case "\x1a":
                return "\\z";
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\"":
            case "'":
            case "\\":
            case "%":
                return "\\" + char; // prepends a backslash to backslash, percent,
                // and double/single quotes
            default:
                return char;
        }
    });
}


export const getCookie = function (name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) {
        return parts.pop().split(";").shift();
    }
    return null;
};

export const isOneOutOfN = function (n, filter = null) {
    if (filter) {
        return filter(n)
    }
    return parseInt(Math.random() * n) === 0;
}

/*
 * example getRandomNumberOutOfRange(n,0) -> any numner between 0..n-1;
 */
export const getRandomNumberOutOfRange = function (n, start = 0) {
    return parseInt(Math.random() * n) + start;
}


export const getUrlParams = (url = window.location.href) => {
    var str = url.split('?')[1];
    if (str) {
        str = str.split('#')[0];
    }
    if (typeof str === 'undefined') {
        str = '';
    }
    var paramsMap = {};
    var strDec = decodeURIComponent(str);
    strDec.replace(
        new RegExp('([^?=&]+)(=([^&]*))?', 'g'),
        function ($0, $1, $2, $3) {
            paramsMap[$1] = $3;
        });
    return paramsMap;
}

/**
 * Converts an object in the form of getUrlParams to a string
 * @param {Map} params - url params map
 */
export const urlParamsToStr = (params) => {
    let ret = "";
    if (!params) {
        return ret;
    }
    for (let [key, value] of Object.entries(params)) {
        ret += `${key}=${value}&`;

    }
    return ret.substring(0, ret.length - 1);
}

