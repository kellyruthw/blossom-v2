/* eslint-disable no-param-reassign */
function observeObject(callback, object, props){
    // eslint-disable-next-line consistent-return
    let t = setInterval( ()=>{
        let tmpArr = [];
        for(let prop of props){
            if(prop in object){
                // eslint-disable-next-line callback-return
                callback(prop);
            }else{
                tmpArr.push(prop);
            }
        }
        props = tmpArr;
        if(props.length < 1){
            clearInterval(t);
        }
        
    },200)


}



export class ObjectObservable {
    constructor(obj, ...props) {
        this.obj = obj;
        
        this.objObsrv = observeObject((prop) => {
            this.notify(prop)
        } , this.obj ,props);
        this.props = {};
        props.forEach((prop) => {
            this.props[prop] = [];
        });

    }

    notify(prop) {
        
        this.props[prop].forEach((callback) => {
            callback();
        });
        this.props[prop] = [];
    }

    get(property) {
        return this.props[property];

    }

    

    push(property , callback ){
        this.props[property].push(callback);
        if(property in this.obj){
            this.notify(property);
        }

    }

}
