<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bestsubscriptions
 */

get_header();
?>

	<div id="primary" class="content-area contact">
		
		<main id="main" class="site-main">

		<div class="content">
				<div class="container">
		
				<?php
				while ( have_posts() ) :
					the_post(); ?>
					
					
		
					<div class="entry-content">
						<h1><?php the_title(); ?></h1>
						<div class="inquiries">
							<div>
								<h2>Media Inquiries</h2>
								<h3>Jocelyn Johnson</h3>
								<a href="mailto:jjohnson@first.media ">jjohnson@first.media</a>
							</div>
							<div>
								<h2>Sales Inquiries</h2>
								<h3>Charles Gabriel</h3>
								<a href="mailto:cgabriel@first.media">cgabriel@first.media</a>
							</div>
						</div>
						<?php the_content(); ?>
						<?php echo do_shortcode("[ninja_form id=1]")?>
					</div><!-- .entry-content -->

		
				<?php
					endwhile; // End of the loop.
				?>
				</div>
			</div>

		</main><!-- #main -->
		
	</div><!-- #primary -->

<?php get_footer(); ?>
