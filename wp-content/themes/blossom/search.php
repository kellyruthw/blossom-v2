<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package bestsubscriptions
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="content">
				<div class="container">
				<h1>Showing results for <span class="search-result"><?php the_search_query(); ?></span></h1>
			
				<div class="articles">
					<?php
					// set the "paged" parameter (use 'page' if the query is on a static front page)
					$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
					$search_term = get_search_query();
					
					$args = array(
					    'posts_per_page' => 12,
					    'paged' => $paged,
					    's' => $search_term
					);
					
					$the_query = new WP_Query($args); ?>
					
					
					<?php
					// the loop
					while ( $the_query->have_posts() ) : $the_query->the_post(); 
					?>
					
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							    <a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_post_thumbnail('feed'); ?></a>
							    <div class="copy">
							    <div class="cat">
								   <?php
									   $categories = get_the_category();
									   $term2 = $categories[0]->term_id;
									   $name = $categories[0]->name;
									   $slug = $categories[0]->slug;
									   $category_link2 = get_category_link( $term2 );
									   $img = get_field('cat_image', 'term_'.$term2);
									?>
									<a href="<?php echo esc_url( $category_link2 ); ?>" class="<?php echo $slug; ?>">
										<img src="<?php echo $img; ?>" alt="<?php echo $img; ?>" /><h4><?php echo $name ?></h4>
									</a>
							    </div>
						        <h2><a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a></h2>
						        <?php the_excerpt(); ?>
						    </div>
						    </article>
					<?php endwhile; ?>
						<div class="navigation">						
							<div class="next">
								<?php next_posts_link( 'Older Entries', $the_query2->max_num_pages ); ?>
							</div>
						</div>
						<?php 
						// clean up after the query and pagination
						wp_reset_postdata(); 
						?>
					</div>
					<?php
					if ( wp_is_mobile() ) { ?>
						<div class="view-more mobile">
							<div class="page-load-status">
							  <div class="loader-ellips infinite-scroll-request">
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							  </div>
							</div>
							<button class="btn">Show me more</button>
						</div>
						
						<?php get_sidebar(); ?>

					<?php } else { ?>
						<?php get_sidebar(); ?>
						<div class="view-more">
							<div class="page-load-status">
							  <div class="loader-ellips infinite-scroll-request">
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							  </div>
							</div>
							<button class="btn">Show me more</button>
						</div>
					<?php }
				?>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
