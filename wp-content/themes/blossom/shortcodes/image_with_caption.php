<?php
function image_with_caption_func( $atts ) {
        $atts = shortcode_atts( array(
            'text'  => '',
            'image' => ''
        ), $atts, 'image_with_caption' );

        $medium_image = wp_get_attachment_image_src( get_attachment_id_by_url($atts['image']), 'full');

        $width  = $medium_image[1];
        $height = $medium_image[2];

        list($width, $height) = getimagesize($atts['image']);

        $html = '<figure class="image-with-captions">
					<img class="lazy image-with-captions__image" data-src="' .  $atts['image'].'?w=450&h=400&fit=crop&crop=entropy&auto=format&q=60' . '" alt=" ">
					<figcaption class="image-with-captions__captions">
						<p>' .  $atts['text'] . '</p>
					</figcaption>
				</figure>';

        return $html;
    }
add_shortcode( 'image_with_caption', 'image_with_caption_func' );







