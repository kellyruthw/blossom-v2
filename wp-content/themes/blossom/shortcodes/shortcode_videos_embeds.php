<?php

function video_quote_func( $atts ) {
    $atts = shortcode_atts( array(
        'title' => '',
        'text'  => '',
        'poster'  => '',
        'pvideo'  => '',
        'video'  => ''
    ), $atts, 'video_quote' );

    $html = 
    '<div class="bu-social-embed bu-social-sy-embed "><div class="video-wrapper" >'.
    '<div class="single-video" data-media-id="'.$atts['video'].'" data-player-id="Igg1JvZW"></div>'.
    '<figcaption  class="video-caption" >'.$atts['text'].'</figcaption>'.
    '</div></div>';

    return $html;
}
add_shortcode( 'video_quote', 'video_quote_func' );


function single_video_func( $atts ) {
    $atts = shortcode_atts( array(
        'title' => '',
        'text'  => '',
        'poster'  => '',
        'pvideo'  => '',
        'video'  => ''
    ), $atts, 'video_quote' );

    $html = '<div class="single-recipe-video" data-media-id="'.$atts['video'].'" data-player-id="Igg1JvZW"></div>';

    return $html;
}
add_shortcode( 'single_video', 'single_video_func' );





?>