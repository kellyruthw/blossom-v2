<aside>
	<div class="search-bar">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-search.svg">
		<?php get_search_form(); ?>
	</div>
	<!-- /2699062/ei_article_m300x250_1 -->
	<div id='div-gpt-ad-1558571663945-0' class="small-ad">
		<script>
		googletag.cmd.push(function() { googletag.display('div-gpt-ad-1558571663945-0'); });
		</script>
	</div>
	<div class="popular-posts">
		<h2>Trending</h2>
	<?php 
		$today = getdate();
		$popularpost = new WP_Query( array( 
		
		'posts_per_page' => 5,
		'meta_key' => 'wpb_post_views_count', 
		'orderby' => 'meta_value_num', 
		'order' => 'DESC'  ) );
		
		while ( $popularpost->have_posts() ) : $popularpost->the_post();
		
		$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );  ?>
 
			 <article>
			    <a href="<?php echo esc_url( get_permalink() ); ?>" class="featured-image"><?php the_post_thumbnail('sidebar-trending'); ?></a>
			    <div class="copy">
			        <h3><a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a></h3>
			    </div>
		    </article>
 
		<?php endwhile; ?>
	</div>
	<?php
		if( !is_singular( 'recipe' ) ) { ?>
		<!-- /2699062/ei_gallery_d300x600_3 -->
		<div id='div-gpt-ad-1558571728730-0' class="tall-ad">
			<script>
			googletag.cmd.push(function() { googletag.display('div-gpt-ad-1558571728730-0'); });
			</script>
		</div>
		<?php } 		
	?>
	

</aside>