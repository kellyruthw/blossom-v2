<?php

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="content main-recipe">
				<div class="container">
					<?php
					while ( have_posts() ) :
					
						global $post;
						
						$postcat = get_the_category( $post->ID );
						$do_not_duplicate[] = $post->ID; 
						
						the_post();
						
						wpb_set_post_views(get_the_ID());
						wpb_get_post_views(get_the_ID());
			
						get_template_part( 'template-parts/content', get_post_type() );
					endwhile; // End of the loop. ?>
					<?php wp_reset_postdata();
					?>
					<?php get_sidebar(); ?>
				</div>
			</div>
			<div class="more-to-love content bg-dotted">
				<div class="container">
					<h2>😍 More to Love in <span>Recipes</span></h2>
					
					<?php
						$catname = $postcat[0]->name;						
						$related = new WP_Query(
						    array(
						        'posts_per_page' => 8,
						        'post__not_in' => $do_not_duplicate,
						        'post_type' => recipe,
						        'orderby' => rand
						    )
						);
						
						if( $related->have_posts() ) { 
						    while( $related->have_posts() ) { 
						        $related->the_post();
						        ?>
						         <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								    <a href="<?php echo esc_url( get_permalink() ); ?>" class="featured-image"><?php the_post_thumbnail('recipe-more'); ?></a>
								    <div class="copy">
								        <h3><a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a></h3>
								    </div>
							    </article>
						    <?php } ?>
						    <?php
							    wp_reset_postdata();
						}
					?>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
