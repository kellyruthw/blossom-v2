<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php bestsubscriptions_post_thumbnail(); ?>
	<div class="entry-content">
		<h2><?php the_title(); ?></h2>
		<?php the_content(); ?>
		<?php the_excerpt(); ?>
	</div>
</article>