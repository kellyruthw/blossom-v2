<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bestsubscriptions
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<h1><?php the_title(); ?></h1>
		<?php the_content(); ?>
		<?php
			if( is_page('contact') ){ 
				echo do_shortcode("[ninja_form id=1]");
		} ?>
		
	</div><!-- .entry-content -->

</div><!-- #post-<?php the_ID(); ?> -->