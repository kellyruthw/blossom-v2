<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bestsubscriptions
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php the_category(); ?>
	<h1><?php the_title(); ?></h1>
		<div class="entry-meta">
			<div class="date">
			By: <span><?php echo get_the_author(); ?></span><?php echo get_the_date(); ?><!--<span><?php the_time( $d ); ?></span>-->
			</div>
		</div><!-- .entry-meta -->
		<?php if( get_field("video_id") ){ ?>			
		
		<?php 
			$videoid = get_field('video_id'); 	
			$url = 'https://cdn.jwplayer.com/players/' .$videoid. '-Igg1JvZW.html';
		?>
		<div class="videoPlayer">
			<div style="position:relative; padding-bottom:100%; overflow:hidden;">
				<iframe src="<?php echo $url; ?>" width="100%" height="100%" frameborder="0" scrolling="auto" allowfullscreen style="position:absolute;"></iframe>
			</div>
		</div>
		<?php } else { ?>
		
		<?php the_post_thumbnail('single'); ?>
			
		<?php } ?>
	
	<div class="contain">
		<div class="social-share">
			<h3>Share</h3>
			<?php
			    global $post;
			    $post_slug = $post->post_name;			    
			?>
			<a href="https://www.facebook.com/sharer/sharer.php?u=https%3A//soyummy2.wpengine.com/<?php echo $post_slug; ?>" data-network="facebook"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-ss-fb.svg"></a>
			<a href="https://twitter.com/home?status=https%3A//everivyv2.wpengine.com/<?php echo $post_slug; ?>/" data-network="twitter"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-ss-tw.svg"></a>
			<a href="https://pinterest.com/pin/create/button/?url=https%3A//everivyv2.wpengine.com/<?php echo $post_slug; ?>/&media=&description=" data-network="pinterest"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-ss-pin.svg"></a>
			<a href="mailto:?body=I%20think%20you'd%20like%20this%20article,%20https%3A//everivyv2.wpengine.com/<?php echo $post_slug; ?>/"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-ss-email.svg"></a>
		</div>
		<div class="entry-content">
			<?php the_content(); ?>
		</div><!-- .entry-content -->
	</div>
	<!--
	<div class="newsletter-signup">
		<div class="copy">
			<h2>Come here often?<div>Subscribe to our newsletter</div></h2>
		</div>
		<div id="mc_embed_signup">
			<form action="https://media.us15.list-manage.com/subscribe/post?u=80a0902385abfbc4f81a6eee7&amp;id=9e32db1dea" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
			    <div id="mc_embed_signup_scroll">
				
				<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
			    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups
			    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_80a0902385abfbc4f81a6eee7_9e32db1dea" tabindex="-1" value=""></div>
			    <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn">
			    </div>
			</form>
		
		</div>
		
		<!--End mc_embed_signup
	</div> -->

</article><!-- #post-<?php the_ID(); ?> -->
