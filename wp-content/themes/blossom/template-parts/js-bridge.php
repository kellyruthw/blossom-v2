<?php

require(__ROOT__.'/fm_utils.php');



    function wp_env_script($wp_env) {
            ?>
<script>
    window.wp_vars = <?php 
            
            echo(json_encode($wp_env)); 
            ?>;
    /**
     * Gets variables from Wordpress and exposes them to the browser
     */
    window.fm = window.fm || {};
    fm.WPEnv = fm.WPEnv || (function () {
        function toBool(strVal) {
            return (strVal == 'true');
        }
        wp_vars.isHomePage = toBool(wp_vars.isHomePage);
        wp_vars.isSponsored = toBool(wp_vars.isSponsored);

        return wp_vars;
    })();
</script>
<?php
    }

    $query = array(
        'post__not_in' => array( get_queried_object_id() ),
        'posts_per_page' => 1
    );
    
    $args = array(
    'url'   => admin_url( 'admin-ajax.php' ),
    'query' => $query,
    );

    
	
    $wp_env = array(
        'pageType' => Utils::getInstance()->getPageType(),
        'services_url' => admin_url('admin-ajax.php'),
        
        'postId' =>  get_the_ID(),
        'isSponsored' => Utils::getInstance()->isSponsored() ? 'true' : 'false',
        'isHomePage' => Utils::getInstance()->isHomePage() ? 'true' : 'false',
        'isMobile' => wp_is_mobile() ? 'true' : 'false',
        'prebidfloordesktop'=> get_option("prebidfloordesktop") , 
        'prebidfloormobile' => get_option("prebidfloormobile"),
        'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
        'query' => $query

        );
    add_action( 'wp_enqueue_scripts', 'wp_env_script', 1, 1 );
    do_action('wp_enqueue_scripts', $wp_env);