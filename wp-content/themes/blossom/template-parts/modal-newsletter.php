<div class="md-modal md-effect-1 " id="modal-1">
	  <div class="md-content">
	    <div class="cta newsletter">
	      <div class="copy">
		    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_template_directory_uri(); ?>/assets/logo-blossom-blue.png" class="logo"></a>
	        <h4>Come here often?</h4>
	        <h2>Subscribe to our newsletter</h2>
	        <div id="mc_embed_signup">
	          <form action="https://media.us15.list-manage.com/subscribe/post?u=80a0902385abfbc4f81a6eee7&amp;id=9e32db1dea" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
	              <div id="mc_embed_signup_scroll">
	          	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
	              <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_80a0902385abfbc4f81a6eee7_9e32db1dea" tabindex="-1" value=""></div>
	              <div class="clear"><input type="submit" value="Sign Up" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
	              </div>
	          </form>
	          <button class="md-close no">No Thanks</button>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>