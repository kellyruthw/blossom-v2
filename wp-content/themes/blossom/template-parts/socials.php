<ul class="socials">
	<li><a href="https://www.facebook.com/FirstMediaBlossom" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-fb.svg"></li></a>
	<li><a href="https://twitter.com/blossomhacks"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-tw.svg"></li></a>
	<li><a href="https://www.instagram.com/blossom/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-ig.svg"></li></a>
	<li><a href="https://www.pinterest.com/blossomhacks/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-pin.svg"></a></li>
	<li><a href="https://www.youtube.com/channel/UC2WuPTt0k8yDJpfenggOAVQ" target=""><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-yt.svg"></a></li>

</ul>
