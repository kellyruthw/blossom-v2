// Require path.
const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

// Configuration object.
const config = {
	
	// Create the entry points.
	// One for frontend and one for the admin area.
	entry: {
		// frontend and admin will replace the [name] portion of the output config below.
		main: './js/main.js',
		//admin: './src/admin/admin-index.js'
	},

	// Create the output files.
	// One for each of our entry points.
	output: {
		// [name] allows for the entry object keys to be used as file names.
		filename: '[name].min.js',
		// Specify the path to the JS files.
		path: path.resolve(__dirname, 'assets/js')
	},

	// Setup a loader to transpile down the latest and great JavaScript so older browsers
	// can understand it.
	module: {
		rules: [{
			// Look for any .js files.
			test: /\.js$/,
			// Exclude the node_modules folder.
			exclude: /node_modules/,
			// Use babel loader to transpile the JS files.
			//loader: 'babel-loader',
			use: [ 'babel-loader'],
			enforce: "pre"
		}]
	},
	plugins: [new CleanWebpackPlugin()]

}

// Export the config object.
module.exports = (env, argv) => {
	if (argv.mode === 'development') {
		config.devtool = 'source-map';
		config.module.rules[0].use.push("source-map-loader");
	}
	return config;
};